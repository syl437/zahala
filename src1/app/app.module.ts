import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {ListPage} from '../pages/list/list';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {LoginPage} from "../pages/login/login";
import {loginService} from "../services/loginService";
import {Config} from "../services/config";
import {HttpModule} from "@angular/http";
import {AllSubstitutePage} from "../pages/all-substitute/all-substitute";
import {UserJobsPage} from "../pages/user-jobs/user-jobs";
import {MessagesPage} from "../pages/messages/messages";
import {TabsPage} from "../pages/tabs/tabs";
import {AddJobModalComponent} from "../components/add-job-modal/add-job-modal";
import {DatePicker} from "@ionic-native/date-picker";
import {JobsService} from "../services/JobsService";
import {RegisterPage} from "../pages/register/register";
import {toastService} from "../services/toastService";
import {substituteService} from "../services/substituteService";
import {OpenJobsPage} from "../pages/open-jobs/open-jobs";
import {NotesPage} from "../pages/notes/notes";
import {AddNoteModalComponent} from "../components/add-note-modal/add-note-modal";
import {sent_to_server_service} from "../services/sent_to_server_service";

@NgModule({
    declarations: [
        MyApp,
        HomePage,
        ListPage,
        LoginPage,
        AllSubstitutePage,
        UserJobsPage,
        MessagesPage,
        TabsPage,
        AddJobModalComponent,
        AddNoteModalComponent,
        RegisterPage,
        OpenJobsPage,
        NotesPage
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp,{tabsPlacement: 'top'}),
        HttpModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        ListPage,
        LoginPage,
        AllSubstitutePage,
        UserJobsPage,
        MessagesPage,
        TabsPage,
        AddJobModalComponent,
        AddNoteModalComponent,
        RegisterPage,
        OpenJobsPage,
        NotesPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        loginService,
        JobsService,
        sent_to_server_service,
        substituteService,
        toastService,
        Config,
        DatePicker,
        {provide: ErrorHandler, useClass: IonicErrorHandler}
    ]
})
export class AppModule {
}
