import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {loginService} from "../../services/loginService";
import {toastService} from "../../services/toastService";
import {UserJobsPage} from "../user-jobs/user-jobs";
import {TabsPage} from "../tabs/tabs";
import {HomePage} from "../home/home";
import {LoginPage} from "../login/login";

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-register',
    templateUrl: 'register.html',
})
export class RegisterPage {

    public info: Object = {
        name: '',
        phone: '',
        address: '',
        info: '',
        password: '',
        mail: '',
        image: '',
        type: '',
        area: ''
    };
    public Regions: any[] = [];
    public Types: any[] = [];
    public emailregex;

    constructor(public navCtrl:NavController, public navParams:NavParams, public Login:loginService, public Toast:toastService) {
        this.Login.getRegions('GetRegions').then(
            (data: any) => {
                console.log("Regions : ", data);
                this.Regions = data;
            });

        this.Login.GetTypes('GetTypes').then(
            (data: any) => {
                console.log("Types : ", data);
                this.Types = data;
            });


    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad RegisterPage');
    }

    doRegister()
    {
        console.log("register response1: ");
        this.emailregex = /\S+@\S+\.\S+/;
        if(this.info['name'].length < 3)
            this.Toast.presentToast('הכנס שם מלא')

                /* else if(this.info.address.length < 3)
            this.Popup.presentAlert( this.translate.instant("missingFieldsText") ,this.translate.instant("missingaddresstext"),'')

        else if(this.info.mail =='')
            this.Popup.presentAlert( this.translate.instant("missingFieldsText") ,this.translate.instant("requiredEmailText"),'')

        else if (!this.emailregex.test(this.info.mail)) {
            this.Popup.presentAlert(this.translate.instant("missingFieldsText"), this.translate.instant("requiredEmailText"),'');
            this.info.mail = '';
        }

        else if(this.info.password =='')
            this.Popup.presentAlert( this.translate.instant("missingFieldsText") ,this.translate.instant("missingpasswordtext"),'')*/

        else
        {
            this.Login.RegisterUser("RegisterUser",this.info).then(data=>{
                if (data == 0) {
                     this.Toast.presentToast('ארעה שגיאה , אנא נסה שנית');
                }
                else {
                    window.localStorage.id = data;
                    this.navCtrl.push(HomePage);
                 }
            });
        }
    }

    GoToLogin()
    {
        this.navCtrl.push(LoginPage);
    }

}
