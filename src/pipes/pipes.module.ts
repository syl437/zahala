import { NgModule } from '@angular/core';
import { FilterJobsPipe } from './filter-jobs/filter-jobs';
@NgModule({
	declarations: [FilterJobsPipe],
	imports: [],
	exports: [FilterJobsPipe]
})
export class PipesModule {}
