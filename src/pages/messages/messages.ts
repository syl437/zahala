import {Component,NgZone} from '@angular/core';
import {App, IonicPage, NavController, NavParams} from 'ionic-angular';
import {ChatService} from "../../services/chatService";
import {ChatPage} from "../chat/chat";
import {Config} from "../../services/config";



/**
 * Generated class for the MessagesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-messages',
    templateUrl: 'messages.html',
})

export class MessagesPage {

    public messages: any[] = [];
    public host;
    public userName;
    public userImage;
    public typeName = '';
    public messagesLen:any;
    
    constructor(public app: App ,public zone: NgZone, public navCtrl: NavController, public navParams: NavParams, public ChatService: ChatService , public Settings: Config) {
        this.ChatService._MessagesArray.subscribe(val => {
            this.zone.run(() => {
                this.messages = val;
                console.log("INN" , this.messages)
            });
        });
    
        this.ChatService._messagesLen.subscribe(val => {
            this.zone.run(() => {
                this.messagesLen = val;
            });
        });

        this.host = this.Settings.host;
        console.log('UserJobsPage');
    
        this.userName = window.localStorage.name;
        this.userImage = this.host+""+window.localStorage.image;
        this.typeName = Settings.getTypeName(window.localStorage.type);
    }

    getMesages()
    {
        this.ChatService.getMessages('getMessages').then((data: any) => {
            console.log("2")
            this.messages = data;
            this.messagesLen = this.messages.length;
            console.log("messages : ", this.messages)
        });
    }

    doRefresh(refresher) {
        this.ChatService.getMessages('getMessages').then((data: any) => {
            console.log("2")
            this.messages = data;
            this.messagesLen = this.messages.length;
            refresher.complete();
        });
    }

    ionViewDidLoad() {
    }

    ionViewWillEnter() {
        console.log('getMesages');
        this.getMesages()
    }

    gotoChat(i) {
        //console.log("chat : " ,this.messages[i].id )
        //this.navCtrl.push(ChatPage, {id: this.messages[i].id, name: this.messages[i].user_name});
        this.app.getRootNav().setRoot(ChatPage,{id: this.messages[i].id, name: this.messages[i].user_name , type:2});
    }

}
