import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,ViewController , LoadingController} from 'ionic-angular';
import {DatePicker} from "@ionic-native/date-picker";
import {toastService} from "../../services/toastService";
import {loginService} from "../../services/loginService";
import {Config} from "../../services/config";
import {JobsService} from "../../services/JobsService";
import {UserJobsPage} from "../../../src/pages/user-jobs/user-jobs";

/**
 * Generated class for the AddJobPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-job',
  templateUrl: 'add-job.html',
})
export class AddJobPage {
    
    public job;
    public date;
    public time;
    public info;
    public Regions: any[] = [];
    public Types: any[] = [];
    public SortType: any[] = [];
    public type;
    public region;
    
    
    constructor(public loadingCtrl:LoadingController, public viewCtrl: ViewController,public navCtrl: NavController,public jobsService:JobsService, public Toast:toastService , private navParams: NavParams, public Login:loginService , public Settings: Config) {
        this.Regions = Settings.Regions;
        this.Types = Settings.Types;
        
        for(let type of this.Types){
            if(type.type == 2)
                this.SortType.push(type);
        }
        console.log("Tp : " , this.Types)
    }
    
    onSubmit(form , type)
    {
        if(type == 1)
        {
            if(form.value.date == undefined)
                this.Toast.presentToast('חובה לבחור תאריך');
            else if(form.value.start_time == undefined)
                this.Toast.presentToast('חובה לבחור שעת התחלה');
            else if(form.value.end_time == undefined)
                this.Toast.presentToast('חובה לבחור שעת סיום');
            else if(form.value.info == undefined)
                this.Toast.presentToast('חובה להזין תיאור המשרה');
            else if(form.value.region == undefined)
                this.Toast.presentToast('חובה לבחור איזור בארץ');
            else if(form.value.type == undefined)
                this.Toast.presentToast('חובה להזין סוג משרה');
            else
            {
                let loading = this.loadingCtrl.create({
                    content: 'המתן בבקשה'
                });
                loading.present();
                let data:any = {'date': form.value.date , 'start_time' : form.value.start_time , 'end_time' : form.value.end_time, 'info' : form.value.info , 'type' : type , 'ganType' : form.value.type  , 'region' : form.value.region};
                this.jobsService.addJob('addJob',data).then((data: any) => {
                    loading.dismiss();
                    this.navCtrl.push(UserJobsPage);
                });
            }
        }
        else
        {
            this.navCtrl.push(UserJobsPage);
        }
    }
    
    ionViewWillEnter() {
        this.job = this.navParams.get('job');
        console.log("TT : " , this.job )
        if(this.job)
        {
            this.date = this.job.Date2;
            this.time = this.job.Time2;
            this.info = this.job.info;
        }
    }

}
