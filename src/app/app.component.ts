import {Component, ViewChild} from '@angular/core';
import {AlertController, Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {HomePage} from '../pages/home/home';
import {ListPage} from '../pages/list/list';
import {LoginPage} from "../pages/login/login";
import {RegisterPage} from "../pages/register/register";
import {Config} from "../services/config";
import {NotesPage} from "../pages/notes/notes";
import {ChatPage} from "../pages/chat/chat";
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import {JobsService} from "../services/JobsService";
import {UserJobsPage} from "../pages/user-jobs/user-jobs";
import {ChatService} from "../services/chatService";
import { Firebase } from '@ionic-native/firebase';
import {loginService} from "../services/loginService";
import { Media, MediaObject } from '@ionic-native/media';



@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild(Nav) nav: Nav;

    rootPage: any = "";//LoginPage;

    pages: Array<{ title: string, component: any , icon:string }>;

    constructor(public platform: Platform,private media: Media,public ChatService: ChatService , public Login:loginService , private firebase: Firebase,public alertCtrl:AlertController,public chatService:ChatService,public loginService:loginService, public jobsService:JobsService, public statusBar: StatusBar, public splashScreen: SplashScreen , public Settings:Config, public push:Push, public alert:AlertController) {
        // used for an example of ngFor and navigation
        
        
        this.pages = [
            {title: 'עמוד ראשי', component: HomePage , icon:'ios-home-outline'},
            {title: 'מי אנחנו', component: HomePage , icon:'ios-people-outline'},
            {title: 'ערוך פרטים אישיים', component: 'InfoPage' , icon:'ios-create-outline'},
            {title: 'תבנית הערות', component: NotesPage , icon:'ios-document-outline'},
            {title: 'התנתק', component: 'LogOut' , icon:'ios-close-circle-outline'}
        ];

        //console.log("Local : ", window.localStorage.id, window.localStorage.sid)
        if (window.localStorage.id == undefined || window.localStorage.id == '')
            this.rootPage = LoginPage; //RegisterPage;//
        else
        {
            this.Login.UpdatePush('UpdatePush');
            this.rootPage = HomePage; // ChatPage; //
        }
        

       // setTimeout(function(){
         //   console.log(chatService)
        //     chatService.getMessageById('getMessageById' , 454).then((data: any) => {
        //         console.log("Message : " , data);
        //         chatService.addPushMessageToArray(data);
        //     });
        // }, 4000);

        this.initializeApp();
    }


    async initializeApp() {
        this.platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            this.statusBar.styleDefault();
            this.splashScreen.hide();
            console.log("ININ")
            if (this.platform.is('cordova')) {
                this.initializeFireBaseAndroid();
            }
            
            this.platform.registerBackButtonAction(() => {

                if (this.nav.length() == 1) {

                    let alert = this.alertCtrl.create({
                        title: 'יציאה מהאפליקצייה',
                        message: 'האם אתה בטוח שברצטנך לצאת מהאפליקצייה',
                        cssClass: 'alertcss',
                        buttons: [
                            {text: "כן", handler: () => {this.platform.exitApp();}},
                            {text: "בטל", role: 'cancel'}
                        ]
                    });
                    alert.present();

                } else
                    this.nav.pop();

            });
            
             // setTimeout(()=>{
             //     this.jobsService.pushJob(161);
             // }, 4000);
        });
    
        try {
            await this.ChatService.getMessages('getMessages');
        } catch (err){
            console.log('checkTokenOnStart', err);
        }
    
    }


    private initializeFireBaseAndroid(): Promise<any> {
        return this.firebase.getToken()
            .catch(error => console.error('Error getting token', error))
            .then(token => {

                this.firebase.subscribe('all').then((result) => {
                    if (result) console.log(`Subscribed to all`);
                    this.subscribeToPushNotificationEvents();
                });
            });
    }

    private initializeFireBaseIos(): Promise<any> {
        return this.firebase.grantPermission()
            .catch(error => console.error('Error getting permission', error))
            .then(() => {
                this.firebase.getToken()
                    .catch(error => console.error('Error getting token', error))
                    .then(token => {

                        console.log(`The token is ${token}`);

                        this.firebase.subscribe('all').then((result) => {
                            if (result) console.log(`Subscribed to all`);

                            this.subscribeToPushNotificationEvents();
                        });
                    });
            })

    }


    private subscribeToPushNotificationEvents(): void {

        // Handle token refresh
        this.firebase.onTokenRefresh().subscribe(
            token => {
                this.Settings.SetUserPush(token);
                this.saveToken(token);
               // this.loginService.sendToken('GetToken' , token).then((data: any) => {console.log("UserDetails : " , data);});
            },
            error => {
                console.error('Error refreshing token', error);
            });

        // Handle incoming notifications
        this.firebase.onNotificationOpen().subscribe(
            (notification) => {

                // !notification.tap
                //     ? alert('The user was using the app when the notification arrived...')
                //     : alert('The app was closed when the notification arrived...');

                if(!notification.tap)
                {
                    if(notification.type == 1) //add job to app
                    {
                        this.jobsService.pushJob(notification.Id);
                    }
                    else if(notification.type == 2) //add job to app
                    {
                        this.chatService.getMessageById('getMessageById' , notification.id).then((data: any) => {
                            this.chatService.addPushMessageToArray(data);
                        });
    
                        const file: MediaObject = this.media.create('https://tapper.co.il/gan/notification.mp3');
                        file.play();
                    }
                    else if(notification.type == 3) //add job to app
                    {
                        this.jobsService.getJobById(notification.Id);
                    }
                }
                else
                {
                    if(notification.type == 1) //add job to app
                        this.jobsService.pushJob(notification.Id);
                    else if(notification.type == 3) //add job to app
                        this.jobsService.getJobById(notification.Id);
                }


                // let notificationAlert = this.alertCtrl.create({
                //     title: notification.title,
                //     message: JSON.stringify(notification),
                //     buttons: ['Ok']
                // });
                // notificationAlert.present();
            },
            error => {
                console.error('Error getting the notification', error);
            });
    }





    private saveToken(token: any): Promise<any> {
        // Send the token to the server
        // console.log('Sending token to the server...');
        return Promise.resolve(true);
    }


    openPage(page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.component == 'LogOut') {
            this.loginService.DeleteToken('DeleteToken' , window.localStorage.id).then((data: any) => {console.log("DeleteToken : " , data);});
            window.localStorage.id = '';
            this.Settings.User = '';
            this.nav.push(LoginPage);
            console.log("Out");
        }
        else
            this.nav.setRoot(page.component);
    }

    pushSetup()
    {
        const options: PushOptions = {
            android: {
                senderID:'473000068631'
            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'false'
            },
            windows: {},
            browser: {
                pushServiceURL: 'http://push.api.phonegap.com/v1/push'
            }
        };

        const pushObject: PushObject = this.push.init(options);


        pushObject.on('notification').subscribe((notification: any) => {
            //alert("Push ")
            // let Message =  notification.additionalData.Message;
            // let Type =  notification.additionalData.Type;
            //
            // if(Type == 1)
            //     this.jobsService.pushJob(Message);
            // if(Type == 2)
            //     this.chatService.pushToArray(Message);
            // if(Type == 3)
            //     this.jobsService.pushOpenJob(Message);
            //
            // if(notification.additionalData.foreground) {
            //
            //
            // }
            // else
            // {
            //     //alert("Notification33333 " + JSON.stringify(notification));
            // }
        });

        pushObject.on('registration').subscribe((registration: any) => {
            this.Settings.SetUserPush(registration.registrationId.toString());
        });

        pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));


    }
}
