import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {ListPage} from '../pages/list/list';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {LoginPage} from "../pages/login/login";
import {loginService} from "../services/loginService";
import {Config} from "../services/config";
import {HttpModule} from "@angular/http";
import {AllSubstitutePage} from "../pages/all-substitute/all-substitute";
import {UserJobsPage} from "../pages/user-jobs/user-jobs";
import {MessagesPage} from "../pages/messages/messages";
import {TabsPage} from "../pages/tabs/tabs";
import {AddJobModalComponent} from "../components/add-job-modal/add-job-modal";
import {DatePicker} from "@ionic-native/date-picker";
import {JobsService} from "../services/JobsService";
import {RegisterPage} from "../pages/register/register";
import {toastService} from "../services/toastService";
import {substituteService} from "../services/substituteService";
import {OpenJobsPage} from "../pages/open-jobs/open-jobs";
import {NotesPage} from "../pages/notes/notes";
import {AddNoteModalComponent} from "../components/add-note-modal/add-note-modal";
import {sent_to_server_service} from "../services/sent_to_server_service";
import {ProjectBidsPage} from "../pages/project-bids/project-bids";
import {ChatPage} from "../pages/chat/chat";
import {ChatService} from "../services/chatService";
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import {Push} from '@ionic-native/push';
import { Firebase } from '@ionic-native/firebase';


import { Camera } from '@ionic-native/camera';
import {Diagnostic} from "@ionic-native/diagnostic";
import {OpenJobs1Page} from "../pages/open-jobs1/open-jobs1";
import {SendBidModalComponent} from "../components/send-bid-modal/send-bid-modal";
import { DataProvider } from '../providers/data/data';
import { Data } from '../providers/data';
import {AddJobPage} from "../pages/add-job/add-job";
import {AutosizeDirective} from "../directives/autosize/autosize";
import { Media, MediaObject } from '@ionic-native/media';
import {PipesModule} from "../pipes/pipes.module";


@NgModule({
    declarations: [
        MyApp,
        HomePage,
        ListPage,
        LoginPage,
        AllSubstitutePage,
        UserJobsPage,
        MessagesPage,
        TabsPage,
        AddJobModalComponent,
        AddNoteModalComponent,
        SendBidModalComponent,
        RegisterPage,
        OpenJobsPage,
        NotesPage,
        ProjectBidsPage,
        OpenJobs1Page,
        AddJobPage,
        ChatPage,
        AutosizeDirective,
        
    ],
    imports: [
        BrowserModule,
        PipesModule,
        IonicModule.forRoot(MyApp,{tabsPlacement: 'top'}),
        HttpModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        ListPage,
        LoginPage,
        AllSubstitutePage,
        UserJobsPage,
        MessagesPage,
        TabsPage,
        AddJobModalComponent,
        AddNoteModalComponent,
        SendBidModalComponent,
        RegisterPage,
        OpenJobsPage,
        OpenJobs1Page,
        NotesPage,
        AddJobPage,
        ProjectBidsPage,
        ChatPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        ChatService,
        loginService,
        JobsService,
        sent_to_server_service,
        substituteService,
        toastService,
        Config,
        DatePicker,
        Camera,
        Diagnostic,
        FileTransfer,
        File,
        Media,
        FilePath,
        Push,
        Data,
        Firebase,
        AutosizeDirective,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
    DataProvider
    ]
})
export class AppModule {
}
