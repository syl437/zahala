import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable, ViewChild} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import {Config} from "./config";
import {Subject} from "rxjs/Subject";
import {Observable} from "rxjs/Observable";
import {Content} from "ionic-angular";
import { Events } from 'ionic-angular';
import {LoadingController} from 'ionic-angular';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';



//const ServerUrl = "http://tapper.co.il/647/laravel/public/api/";


@Injectable()

export class ChatService
{
    public ServerUrl;
    ChatArray:any[] = [];
    public _ChatArray = new Subject<any>();
    ChatArray$: Observable<any> = this._ChatArray.asObservable();

    //MessagesArray:any[] = [];
    public _MessagesArray: BehaviorSubject<any> = new BehaviorSubject(null);
    readonly MessagesArray$: Observable<any> = this._MessagesArray.asObservable();
    get MessagesArray(): Array<any> {return this._MessagesArray.getValue();}
    
   // public _MessagesArray = new Subject<any>();
   // _MessagesArray$: Observable<any> = this._MessagesArray.asObservable();
    
    public _messagesLen: BehaviorSubject<number> = new BehaviorSubject(null);
    readonly messagesLen$: Observable<number> = this._messagesLen.asObservable();
    get messagesLen(): number {return this._messagesLen.getValue();}
    
    // public _messagesLen: BehaviorSubject<any> = new BehaviorSubject(null);
    // messagesLen$: Observable<any> = this._messagesLen.asObservable();
    // get messagesLen():any {return this._messagesLen.getValue();}

    Obj:any;
    public date:any;
    public hours:any;
    public minutes:any;
    public seconds:any;
    public time:any;
    public today:any;
    public dd:any;
    public mm:any;
    public yyyy:any;
    public newdate:any;

    @ViewChild(Content) content: Content;

    constructor(private http:Http,
                public Settings:Config,
                public events: Events,
                public loadingCtrl: LoadingController) {
        
        this.ServerUrl = Settings.ServerUrl;
        console.log(this.ChatArray)
    }

    addTitle(url:string , obj)
    {
        let body = 'info=' + JSON.stringify(obj);
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });

        let options = new RequestOptions({
            headers: headers
        });

        return this.http.post(this.ServerUrl + '' + url, body, options).map(res => res).do((data)=>{console.log("Chat : " , data)}).toPromise();
    }
    
    updateReadMessage(url,sender,reciver)
    {
        let body = new FormData();
        body.append('sender_id', sender);
        body.append('reciver_id', reciver );
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res).do((data)=>{}).toPromise();
    }

    getChatDetails(url:string,reciverId)
    {
        let body = new FormData();
        body.append('sender_id', window.localStorage.id );
        body.append('reciver_id', reciverId );
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{this.updateChatArray(data)}).toPromise();
    }

    updateChatArray(data)
    {
        console.log("00002")
        this.ChatArray = data;
        this.ChatArray.reverse();
        this._ChatArray.next(this.ChatArray);
    }

    pushToArray(data)
    {
         this.ChatArray.splice((this.ChatArray.length),0,data);
         this._ChatArray.next(this.ChatArray);
         console.log(this.ChatArray , this.ChatArray.length)
    }

    registerPush(url,pushid)
    {
        let body = new FormData();
        body.append('push_id', pushid.toString() );
        body.append('user_id', window.localStorage.identify.toString() );
        return this.http.post(this.ServerUrl + '' + url, body).map(res => '').do((data)=>{console.log("chat:" , data)});
    }

    getChatMessagesCount(url)
    {
        let body = new FormData();
        body.append('user_id', window.localStorage.identify.toString() );
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{return data;}).toPromise();
    }

    // getMessages(url)
    // {
    //     let body = new FormData();
    //     body.append('uid', window.localStorage.id);
    //
    //     return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{
    //         this.MessagesArray = data;
    //         this._MessagesArray.next(this.MessagesArray);
    //     }).toPromise();
    // }
    
    // Get all categories ;
    async getMessages(url): Promise<Array<any>> {
        return new Promise<Array<any>>(async (resolve, reject) => {
            let loading = this.loadingCtrl.create({content: 'Please wait...'});
            loading.present();
            
            try {
                console.log("Local : ", window.localStorage.id)
                let body = new FormData();
                body.append('uid',window.localStorage.id);
                let MessagesArray = await this.http.post(this.ServerUrl + '' + url, body).toPromise().then(response => response.json());
                this._MessagesArray.next(MessagesArray);
                resolve(this.MessagesArray);
            } catch (err) {
                console.log(err);
                reject(err);
            } finally {
                loading.dismiss();
                this.calculateMessagesLength();
            }
        });
    }
    

    getMessageById(url,id)
    {
        let body = new FormData();
        body.append('id', id);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{return data;}).toPromise();
    }

    addPushMessageToArray(data)
    {
        data.unread_message = 0;
        this.Obj = data; //{ id:'', uid:3 , title:data.title , date:this.newdate , type:1,name:data.SenderName,image:data.SenderImage};
        this.pushToArray(this.Obj);
        this.events.publish('newchat',"newchat");
        
        let sum:number = this.messagesLen;
        sum = Number(sum)+1;
        this._messagesLen.next(sum);
    }
    
    calculateMessagesLength()
    {
        console.log("MessagesArray : " ,this.MessagesArray)
        let sum = 0;
        
        for(let item of this.MessagesArray){
                sum+= Number(item.unread_message);
        }
        this._messagesLen.next(sum);
        
        // return new Promise<void>(async (resolve, reject) => {
        //     this._messagesLen.next(sum);
        // });
        
    }
};


