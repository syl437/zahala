
import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import {Config} from "./config";
import {HomePage} from "../pages/home/home";


@Injectable()

export class substituteService
{
    public ServerUrl;

    constructor(private http:Http,public Settings:Config) {
        this.ServerUrl = Settings.ServerUrl;
    };

    getAllSubstitute(url:string,type)
    {
        let body = new FormData();
        body.append('type', type);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{ }).toPromise();
    }
};


