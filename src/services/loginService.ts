
import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import {Config} from "./config";
import {HomePage} from "../pages/home/home";


@Injectable()

export class loginService
{
    public ServerUrl;

    constructor(private http:Http,public Settings:Config) {
        this.ServerUrl = Settings.ServerUrl;
    };

    getUserDetails(url:string , info:Object)
    {
        //console.log("Info : " , info)
        let body = new FormData();
        body.append('mail',info["mail"]);
        body.append('password',info["password"]);
        body.append('push',this.Settings.PushId);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{console.log("getUserDetails : " , data)}).toPromise();
    }

    logOutUser()
    {
        window.localStorage.identify = '';
        this.Settings.UserId = 0;
    }

    getRegions(url:string)
    {
        let body = new FormData();
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{}).toPromise();
    }

    GetTypes(url:string)
    {
        let body = new FormData();
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{}).toPromise();
    }

    RegisterUser(url:string , info:Object)
    {
        let body = 'info=' + JSON.stringify(info);
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });

        let options = new RequestOptions({
            headers: headers
        });

        return this.http.post(this.ServerUrl + '' + url, body, options).map(res => res.json()).do((data)=>{ }).toPromise();
    }
    
    
    UpdateUser(url:string , info:Object)
    {
        let body = 'info=' + JSON.stringify(info);
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
    
        let options = new RequestOptions({
            headers: headers
        });
    
        return this.http.post(this.ServerUrl + '' + url, body, options).map(res => res.json()).do((data)=>{ }).toPromise();
    }

    getUserById(url:string)
    {
        //window.localStorage.id = 14;
        console.log("JobssssId : " , window.localStorage.id)
        let body = new FormData();
        body.append('uid',window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{}).toPromise();
    }

    DeleteToken(url:string , id)
    {
        let body = new FormData();
        body.append('id',id);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res).do((data)=>{}).toPromise();
    }
    
    UpdatePush(url:string)
    {
        let body = new FormData();
        body.append('id',window.localStorage.id);
        body.append('push',this.Settings.PushId);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{console.log("getUserDetails : " , data)}).toPromise();
    }

};


