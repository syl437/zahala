import { Component } from '@angular/core';
import {toastService} from "../../services/toastService";
import {NavParams,ViewController} from "ionic-angular";
import {Config} from "../../services/config";

/**
 * Generated class for the SendBidModalComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'send-bid-modal',
  templateUrl: 'send-bid-modal.html'
})
export class SendBidModalComponent {

    public info;
    public title1:string = "ss";

    constructor(public viewCtrl: ViewController,private navParams: NavParams , public Toast:toastService , public Settings: Config) {
      this.title1 = window.localStorage.info;
    }

    onSubmit(form ,type)
    {
        console.log("tt : " , form.value.title)
        if(type == 1)
        {
            if(form.value.title == undefined)
                this.Toast.presentToast('חובה להזין תיאור משרה');
            else
            {
                let data = {'title': form.value.title  ,'type':type};
                this.viewCtrl.dismiss(data);
            }
        }
        else
        {
            let data = {'title': form.value.title  , 'info' : form.value.info ,'type':type};
            this.viewCtrl.dismiss(data);
        }
    }



    ionViewWillEnter() {
        this.info = this.navParams.get('info');
        this.title1 = this.navParams.get('title');
        console.log("TT : " , this.info + " : " + this.title1)
    }

}
