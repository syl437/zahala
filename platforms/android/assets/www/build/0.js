webpackJsonp([0],{

/***/ 712:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InfoPageModule", function() { return InfoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__info__ = __webpack_require__(722);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var InfoPageModule = (function () {
    function InfoPageModule() {
    }
    InfoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__info__["a" /* InfoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__info__["a" /* InfoPage */]),
            ],
        })
    ], InfoPageModule);
    return InfoPageModule;
}());

//# sourceMappingURL=info.module.js.map

/***/ }),

/***/ 722:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InfoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_loginService__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_toastService__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(49);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the InfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InfoPage = (function () {
    function InfoPage(navCtrl, navParams, Toast, Login) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Toast = Toast;
        this.Login = Login;
        this.info = {
            id: '',
            name: '',
            phone: '',
            address: '',
            info: '',
            password: '',
            mail: '',
            image: '',
            type: '',
            area: ''
        };
        this.serverImage = '';
        this.Regions = [];
        this.Types = [];
        this.lastImage = null;
        this.Login.getRegions('GetRegions').then(function (data) {
            console.log("Regions : ", data);
            _this.Regions = data;
        });
        this.Login.GetTypes('GetTypes').then(function (data) {
            console.log("Types : ", data);
            _this.Types = data;
        });
        this.Login.getUserById('getUserById').then(function (data) {
            console.log("Types : ", data[0]);
            _this.info = data[0];
            _this.info['area'] = data[0].area_id;
            _this.info['type'] = data[0].type_id;
        });
    }
    InfoPage.prototype.doRegister = function () {
        var _this = this;
        this.emailregex = /\S+@\S+\.\S+/;
        if (this.info['name'].length < 3)
            this.Toast.presentToast('הכנס שם מלא');
        else if (this.info['phone'].length < 8)
            this.Toast.presentToast('הכנס מספר טלפון חוקי');
        else if (!this.emailregex.test(this.info['mail'])) {
            this.info['mail'] = '';
            this.Toast.presentToast('מייל לא תקין');
        }
        else if (this.info['address'].length < 2)
            this.Toast.presentToast('הכנס כתובת חוקית');
        else if (this.info['password'].length < 3)
            this.Toast.presentToast('הכנס סיסמה תקינה');
        else if (this.info['area'] == '')
            this.Toast.presentToast('בחר איזור');
        else if (this.info['type'] == '')
            this.Toast.presentToast('בחר סוג תפקיד');
        else {
            this.info['image'] = this.serverImage;
            this.Login.UpdateUser("UpdateUser", this.info).then(function (data) {
                if (data == 0) {
                    _this.Toast.presentToast('ארעה שגיאה , אנא נסה שנית');
                }
                else {
                    console.log("Response : ", data.name);
                    _this.info = data;
                    _this.info['area'] = data.area_id;
                    _this.info['type'] = data.type_id;
                    window.localStorage.name = _this.info['name'];
                    window.localStorage.area = _this.info["area_id"];
                    window.localStorage.id = _this.info["id"];
                    window.localStorage.name = _this.info["name"];
                    window.localStorage.info = _this.info["info"];
                    window.localStorage.image = _this.info["image"];
                    window.localStorage.type = _this.info["type_id"];
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
                }
            });
        }
    };
    InfoPage.prototype.GoToHome = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
    };
    InfoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad InfoPage');
    };
    InfoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-info',template:/*ion-inline-start:"C:\Users\USER\Documents\github\zahala\src\pages\info\info.html"*/'<ion-content padding>\n\n    <div class="loginMain">\n\n        <div class="loginTitle">\n\n            הרשמה\n\n        </div>\n\n        <div class="InputsText" align="center">\n\n            <div class="InputText">\n\n                <input type="text" placeholder="עדכן שם מלא" [(ngModel)]="info.name" />\n\n            </div>\n\n            <div class="InputText mt10">\n\n                <input type="text" placeholder="הכנס מספר טלפון" [(ngModel)]="info.phone" />\n\n            </div>\n\n            <div class="InputText mt10">\n\n                <input type="text" placeholder="הכנס כתובת מדוייקת" [(ngModel)]="info.address" />\n\n            </div>\n\n            <div class="InputText mt10">\n\n                <input type="text" placeholder="הכנס אימייל" [(ngModel)]="info.mail" />\n\n            </div>\n\n            <div class="InputText mt10">\n\n                <input type="text" placeholder="הכנס סיסמה" [(ngModel)]="info.password" />\n\n            </div>\n\n\n\n            <ion-item class="selectRegion">\n\n                <ion-label>בחר איזור</ion-label>\n\n                <ion-select [(ngModel)]="info.area" >\n\n                    <ion-option value="{{region.id}}" *ngFor="let region of Regions">{{region.name}}</ion-option>\n\n                </ion-select>\n\n            </ion-item>\n\n\n\n            <ion-item class="selectRegion">\n\n                <ion-label>בחר סוג</ion-label>\n\n                <ion-select [(ngModel)]="info.type">\n\n                    <ion-option value="{{type.id}}" *ngFor="let type of Types">{{type.name}}</ion-option>\n\n                </ion-select>\n\n            </ion-item>\n\n\n\n            <div class="InputText mt10">\n\n                <textarea [(ngModel)]="info.info" name="info" placeholder="הכניסי קצת פרטים עלייך" rows="4"></textarea>\n\n            </div>\n\n\n\n            <button class="loginButton" ion-button block (click)="doRegister()">שמור</button>\n\n            <a class="register" (click)="GoToHome()">חזור לעמוד ראשי  </a>\n\n        </div>\n\n    </div>\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\zahala\src\pages\info\info.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__services_toastService__["a" /* toastService */], __WEBPACK_IMPORTED_MODULE_2__services_loginService__["a" /* loginService */]])
    ], InfoPage);
    return InfoPage;
}());

//# sourceMappingURL=info.js.map

/***/ })

});
//# sourceMappingURL=0.js.map