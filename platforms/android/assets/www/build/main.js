webpackJsonp([12],{

/***/ 16:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Config; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UserId = "http://tapper.co.il/647/laravel/public/api/";
var Config = (function () {
    function Config() {
        this.UserId = '';
        this.PushId = '';
        this.ServerUrl = 'http://tapper.co.il/gan/laravel/public/api/';
        this.host = 'http://tapper.co.il/gan/laravel/storage/app/public/';
        this.CourseId = '';
        this.Substitute = 0;
        this.JobsCount = 0;
        this.openJobs = 2;
        this.openJobsType3 = 3;
        this.openJobsType4 = 4;
        this.TotalSub3 = 3;
        this.TotalSub4 = 4;
        this.Regions = [];
        this.Types = [];
    }
    ;
    Config.prototype.SetUserPush = function (push_id) {
        this.PushId = push_id;
        window.localStorage.push_id = push_id;
    };
    Config.prototype.getTypeName = function (id) {
        var name = '';
        if (id == 1)
            name = "גננת";
        if (id == 3)
            name = "סייעת";
        if (id == 3)
            name = "גננת מחליפה";
        if (id == 4)
            name = "סייעת מחליפה";
        return name;
    };
    Config = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], Config);
    return Config;
}());

;
//# sourceMappingURL=config.js.map

/***/ }),

/***/ 166:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddJobPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_toastService__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_loginService__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_config__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_JobsService__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__src_pages_user_jobs_user_jobs__ = __webpack_require__(70);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the AddJobPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AddJobPage = (function () {
    function AddJobPage(loadingCtrl, viewCtrl, navCtrl, jobsService, Toast, navParams, Login, Settings) {
        this.loadingCtrl = loadingCtrl;
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.jobsService = jobsService;
        this.Toast = Toast;
        this.navParams = navParams;
        this.Login = Login;
        this.Settings = Settings;
        this.Regions = [];
        this.Types = [];
        this.SortType = [];
        this.Regions = Settings.Regions;
        this.Types = Settings.Types;
        for (var _i = 0, _a = this.Types; _i < _a.length; _i++) {
            var type = _a[_i];
            if (type.type == 2)
                this.SortType.push(type);
        }
        console.log("Tp : ", this.Types);
    }
    AddJobPage.prototype.onSubmit = function (form, type) {
        var _this = this;
        if (type == 1) {
            if (form.value.date == undefined)
                this.Toast.presentToast('חובה לבחור תאריך');
            else if (form.value.start_time == undefined)
                this.Toast.presentToast('חובה לבחור שעת התחלה');
            else if (form.value.end_time == undefined)
                this.Toast.presentToast('חובה לבחור שעת סיום');
            else if (form.value.info == undefined)
                this.Toast.presentToast('חובה להזין תיאור המשרה');
            else if (form.value.region == undefined)
                this.Toast.presentToast('חובה לבחור איזור בארץ');
            else if (form.value.type == undefined)
                this.Toast.presentToast('חובה להזין סוג משרה');
            else {
                var loading_1 = this.loadingCtrl.create({
                    content: 'המתן בבקשה'
                });
                loading_1.present();
                var data = { 'date': form.value.date, 'start_time': form.value.start_time, 'end_time': form.value.end_time, 'info': form.value.info, 'type': type, 'ganType': form.value.type, 'region': form.value.region };
                this.jobsService.addJob('addJob', data).then(function (data) {
                    loading_1.dismiss();
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__src_pages_user_jobs_user_jobs__["a" /* UserJobsPage */]);
                });
            }
        }
        else {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__src_pages_user_jobs_user_jobs__["a" /* UserJobsPage */]);
        }
    };
    AddJobPage.prototype.ionViewWillEnter = function () {
        this.job = this.navParams.get('job');
        console.log("TT : ", this.job);
        if (this.job) {
            this.date = this.job.Date2;
            this.time = this.job.Time2;
            this.info = this.job.info;
        }
    };
    AddJobPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-add-job',template:/*ion-inline-start:"C:\Users\USER\Documents\github\zahala\src\pages\add-job\add-job.html"*/'<!--\n\n  Generated template for the AddJobPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <!--<ion-title>addJob</ion-title>-->\n\n      <ion-title align="center" class="HeaderTitle2">הוסיפי משרה </ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n\n\n<ion-content padding>\n\n    <div class="Content">\n\n\n\n        <div class="title">\n\n            הכנסת משרה\n\n        </div>\n\n\n\n        <form #f="ngForm" class="Form"  >\n\n            <ion-list>\n\n\n\n                <ion-item class="formElement">\n\n                    <ion-label>בחר תאריך</ion-label>\n\n                    <ion-datetime displayFormat="DD/MM/YYYY" [(ngModel)]="date" name="date"></ion-datetime>\n\n                </ion-item>\n\n\n\n                <ion-item class="formElement">\n\n                    <ion-label>בחרי שעת התחלה</ion-label>\n\n                    <ion-datetime displayFormat="HH:mm " pickerFormat="HH mm " [(ngModel)]="start_time" name="start_time"></ion-datetime>\n\n                </ion-item>\n\n\n\n                <ion-item class="formElement">\n\n                    <ion-label>בחרי שעת סיום</ion-label>\n\n                    <ion-datetime displayFormat="HH:mm " pickerFormat="HH mm " [(ngModel)]="end_time" name="end_time"></ion-datetime>\n\n                </ion-item>\n\n\n\n                <ion-item>\n\n                    <ion-label>בחרי איזור</ion-label>\n\n                    <ion-select [(ngModel)]="region" name="region">\n\n                        <ion-option value="{{type.id}}" *ngFor="let type of Regions">{{type.name}}</ion-option>\n\n                    </ion-select>\n\n                </ion-item>\n\n\n\n                <ion-item>\n\n                    <ion-label>בחרי סוג</ion-label>\n\n                    <ion-select [(ngModel)]="type" name="type">\n\n                        <ion-option value="{{type.id}}" *ngFor="let type of SortType">{{type.name}}</ion-option>\n\n                    </ion-select>\n\n                </ion-item>\n\n\n\n                <ion-item>\n\n                    <ion-textarea [(ngModel)]="info" name="info" placeholder="הכניסי תיאור המשרה"></ion-textarea>\n\n                </ion-item>\n\n            </ion-list>\n\n\n\n            <div style="width: 100%; margin-top: 50px;" align="center">\n\n                <div style="width: 80%" align="center">\n\n                    <button ion-button block (click)="onSubmit(f,1)">הוסף משרה </button>\n\n                    <button ion-button block (click)="onSubmit(f,0)" style="background-color: red">בטל  </button>\n\n                </div>\n\n            </div>\n\n\n\n        </form>\n\n    </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\zahala\src\pages\add-job\add-job.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_5__services_JobsService__["a" /* JobsService */], __WEBPACK_IMPORTED_MODULE_2__services_toastService__["a" /* toastService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__services_loginService__["a" /* loginService */], __WEBPACK_IMPORTED_MODULE_4__services_config__["a" /* Config */]])
    ], AddJobPage);
    return AddJobPage;
}());

//# sourceMappingURL=add-job.js.map

/***/ }),

/***/ 167:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProjectBidsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__chat_chat__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_config__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the ProjectBidsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProjectBidsPage = (function () {
    function ProjectBidsPage(navCtrl, navParams, Settings) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Settings = Settings;
        this.Bids = [];
        this.Bids = this.navParams.get('project');
        this.host = Settings.host;
        console.log(this.Bids);
        console.log('ProjectBidsPage');
    }
    ProjectBidsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProjectBidsPage');
    };
    ProjectBidsPage.prototype.goBack = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */], { SelectedIndex: 1 });
    };
    ProjectBidsPage.prototype.gotoChat = function (i) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__chat_chat__["a" /* ChatPage */], { id: this.Bids[i].user[0].id, name: this.Bids[i].user[0].name, type: 1 });
    };
    ProjectBidsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-project-bids',template:/*ion-inline-start:"C:\Users\USER\Documents\github\zahala\src\pages\project-bids\project-bids.html"*/'<!--\n\n  Generated template for the ProjectBidsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation. navPop\n\n-->\n\n<ion-header xmlns="http://www.w3.org/1999/html">\n\n\n\n  <ion-navbar hideBackButton >\n\n    <ion-title align="center" class="HeaderTitle"> צהלה <span class="HeaderSubTitle">הצעות לפרוייקט</span> </ion-title>\n\n    <ion-buttons left>\n\n      <button ion-button (click)="goBack()" icon-only>\n\n        <ion-icon ios="ios-arrow-back" md="md-arrow-back"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <div>\n\n    <ion-list>\n\n      <ion-item  text-wrap detail-push *ngFor="let bid of Bids let i=index" side="right" style="direction:rtl">\n\n        <div class="jobRight">\n\n          <img *ngIf="bid.user[0].image == \'\'" class="thumb" src="https://wordsmith.org/words/images/avatar2_large.png" style="width: 100%" />\n\n          <img *ngIf="bid.user[0].image != \'\'" class="thumb" src="{{host}}{{bid.user[0].image}}" style="width: 100%" />\n\n        </div>\n\n        <div class="jobLeft">\n\n          <h2>{{bid.user[0].name}}</h2>\n\n          <h3>{{bid.info}}</h3>\n\n          <button ion-button style="direction: rtl" (click)="gotoChat(i)">שלחי הודעה ל{{bid.user[0].name}}</button>\n\n        </div>\n\n      </ion-item>\n\n    </ion-list>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\zahala\src\pages\project-bids\project-bids.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__services_config__["a" /* Config */]])
    ], ProjectBidsPage);
    return ProjectBidsPage;
}());

//# sourceMappingURL=project-bids.js.map

/***/ }),

/***/ 168:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OpenJobsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_JobsService__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_config__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_sent_to_server_service__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_send_bid_modal_send_bid_modal__ = __webpack_require__(324);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the OpenJobsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var OpenJobsPage = (function () {
    function OpenJobsPage(navCtrl, modalCtrl, zone, navParams, jobsService, Settings, serverService, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.zone = zone;
        this.navParams = navParams;
        this.jobsService = jobsService;
        this.Settings = Settings;
        this.serverService = serverService;
        this.alertCtrl = alertCtrl;
        this.CurrentJob = '';
        this.Notes = [];
        this.Regions = [];
        this.Types = [];
        this.SortType = [];
        this.Area = window.localStorage.area;
        this.Type = '';
        this.host = '';
        this.TotalJob3 = 0;
        this.TotalJob4 = 0;
        this.typeName = '';
        this.Regions = Settings.Regions;
        this.Types = Settings.Types;
        this.host = Settings.host;
        this.userName = window.localStorage.name;
        this.userImage = this.host + "" + window.localStorage.image;
        this.typeName = Settings.getTypeName(window.localStorage.type);
        this.jobs = this.jobsService.OpenJobs;
        for (var _i = 0, _a = this.Types; _i < _a.length; _i++) {
            var type = _a[_i];
            if (type.type == 2)
                this.SortType.push(type);
        }
        this.jobsService._OpenJobs.subscribe(function (data) {
            _this.zone.run(function () {
                console.log("SubScribe : ", data);
                _this.jobs = data;
                _this.getTotalJobs();
            });
        });
        console.log("Jobs : ", this.SortType, this.Regions);
        this.serverService.getAllNotes('getAllNotes').then(function (data) {
            _this.Notes = data;
        });
    }
    OpenJobsPage.prototype.ionViewDidLoad = function () {
        this.getTotalJobs();
    };
    OpenJobsPage.prototype.getTotalJobs = function () {
        this.TotalJob3 = 0;
        this.TotalJob4 = 0;
        for (var i = 0; i < this.jobs.length; i++) {
            if (this.jobs[i].type == 3)
                this.TotalJob3++;
            else
                this.TotalJob4++;
        }
    };
    OpenJobsPage.prototype.ionViewWillEnter = function () {
        this.Type = this.navParams.data;
        this.Settings.openJobsType3 = this.TotalJob3;
        this.Settings.openJobsType4 = this.TotalJob4;
    };
    OpenJobsPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        this.jobsService.getAllOpenJobs('getAllOpenJobs').then(function (data) {
            _this.jobs = data;
            _this.Settings.openJobs = data.length;
            refresher.complete();
        });
    };
    OpenJobsPage.prototype.reversDate = function (dt) {
        var arr = dt.split('-');
        var str = arr[2] + "/" + arr[1] + "/" + arr[0];
        return str;
    };
    OpenJobsPage.prototype.presentPrompt = function (i) {
        var _this = this;
        this.CurrentJob = this.jobs[i].id;
        var noteModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__components_send_bid_modal_send_bid_modal__["a" /* SendBidModalComponent */]);
        noteModal.present();
        noteModal.onDidDismiss(function (data) {
            if (data.type == 1) {
                console.log(data.title + " : " + _this.CurrentJob);
                _this.serverService.addBid('addBid', _this.CurrentJob, data.title).then(function (data) {
                    _this.jobs[i].isBid = 1;
                });
            }
        });
    };
    OpenJobsPage.prototype.cutTime = function (time) {
        var tm = time.split(":");
        return tm[0] + ":" + tm[1];
    };
    OpenJobsPage.prototype.getCurrentDate = function (date) {
        var tm = date.split(" ");
        var tm1 = tm[0].split("-");
        return tm1[2] + "/" + tm1[1] + "/" + tm1[0];
    };
    OpenJobsPage.prototype.getCurrentTime = function (date) {
        var tm = date.split(" ");
        var tm1 = tm[1].split(":");
        return tm1[0] + ":" + tm1[1];
    };
    OpenJobsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-open-jobs',template:/*ion-inline-start:"C:\Users\USER\Documents\github\zahala\src\pages\open-jobs\open-jobs.html"*/'<!--\n\n  Generated template for the OpenJobsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header xmlns="http://www.w3.org/1999/html">\n\n  <ion-navbar >\n\n    <button class="HeaderMenuButton"  ion-button icon-only menuToggle start>\n\n      <ion-icon name="md-menu"></ion-icon>\n\n    </button>\n\n    <ion-title align="center" class="HeaderTitle1">\n\n      <img class="headerImage" src="{{userImage}}"  />\n\n      <div class="HeaderTitleText"> {{userName }}</div>\n\n      <div class="HeaderTitleDesc"> {{typeName }}</div>\n\n   </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n\n    <ion-refresher-content></ion-refresher-content>\n\n  </ion-refresher>\n\n  <div>\n\n    <div class="filter">\n\n      <div>\n\n        <!--<ion-item class="selectRegion1">-->\n\n          <!--<ion-label>בחרי איזור</ion-label>-->\n\n          <!--<ion-select [(ngModel)]="Area">-->\n\n            <!--<ion-option style="margin-left: 30px; padding-left: 30px; background-color:red !important;" value="{{area.id}}" *ngFor="let area of Regions">{{area.name}}</ion-option>-->\n\n          <!--</ion-select>-->\n\n        <!--</ion-item>-->\n\n          <div class="item item-select selectRegion2">\n\n              <select [(ngModel)]="Area">\n\n                  <option style="margin-left: 30px; padding-left: 30px; " value="{{area.id}}" *ngFor="let area of Regions">{{area.name}}</option>\n\n              </select>\n\n          </div>\n\n      </div>\n\n    </div>\n\n    <ion-list>\n\n      <div *ngFor="let job of jobs | filterJobs:Area let i=index  ">\n\n        <ion-item  text-wrap detail-push  *ngIf="job.type == Type" side="right" style="direction:rtl">\n\n          <div >\n\n            <div class="jobRight">\n\n              <img *ngIf="job.Image == \'\'" class="thumb" src="https://wordsmith.org/words/images/avatar2_large.png" style="width: 100%" />\n\n              <img *ngIf="job.Image != \'\'" class="thumb" src="{{host}}{{job.Image}}" style="width:80px; height:73px;" />\n\n            </div>\n\n            <div class="jobLeft">\n\n              <div class="time">\n\n                <div class="timeLeft">\n\n\n\n                  <h3>  {{getCurrentDate(job.created_at)}}  </h3>\n\n                </div>\n\n                <div class="timeRight">\n\n                  <h3 style="direction: rtl">שעה : {{getCurrentTime(job.created_at)}}</h3>\n\n                </div>\n\n                <div class="edit" (click)="updateModal(i)">\n\n                  <img src="images/edit.png" style="width: 100%" />\n\n                </div>\n\n              </div>\n\n              <hr style="margin:0px;">\n\n                <h2>{{job.info}}</h2>\n\n                <h2>תאריך : {{reversDate(job.Date2)}} </h2>\n\n              <div class="time">\n\n                <div class="timeLeft">\n\n                  <h3>התחלה : {{job.Time2}}</h3>\n\n                </div>\n\n                <div class="timeRight">\n\n                  <h3 style="direction: rtl">סיום : {{cutTime(job.end_time)}}</h3>\n\n                </div>\n\n              </div>\n\n\n\n\n\n\n\n              <button style="background-color: red" *ngIf="job.isBid != 1" ion-button (click)="presentPrompt(i)">הגישי הצעה</button>\n\n              <button *ngIf="job.isBid == 1" ion-button>שלחת הצעה לעבודה</button>\n\n            </div>\n\n          </div>\n\n        </ion-item>\n\n      </div>\n\n\n\n    </ion-list>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\zahala\src\pages\open-jobs\open-jobs.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_JobsService__["a" /* JobsService */], __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_4__services_sent_to_server_service__["a" /* sent_to_server_service */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], OpenJobsPage);
    return OpenJobsPage;
}());

//# sourceMappingURL=open-jobs.js.map

/***/ }),

/***/ 169:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_loginService__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_file_transfer__ = __webpack_require__(326);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_toastService__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__home_home__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__login_login__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_camera__ = __webpack_require__(327);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_diagnostic__ = __webpack_require__(328);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_file__ = __webpack_require__(329);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_file_path__ = __webpack_require__(330);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RegisterPage = (function () {
    function RegisterPage(navCtrl, navParams, Login, Toast, camera, diagnostic, transfer, file, filePath, actionSheetCtrl, toastCtrl, platform, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Login = Login;
        this.Toast = Toast;
        this.camera = camera;
        this.diagnostic = diagnostic;
        this.transfer = transfer;
        this.file = file;
        this.filePath = filePath;
        this.actionSheetCtrl = actionSheetCtrl;
        this.toastCtrl = toastCtrl;
        this.platform = platform;
        this.loadingCtrl = loadingCtrl;
        this.info = {
            name: '',
            phone: '',
            address: '',
            info: '',
            password: '',
            mail: '',
            image: '',
            type: '',
            area: ''
        };
        this.serverImage = '';
        this.Regions = [];
        this.Types = [];
        this.lastImage = null;
        this.options = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        this.Login.getRegions('GetRegions').then(function (data) {
            console.log("Regions : ", data);
            _this.Regions = data;
        });
        this.Login.GetTypes('GetTypes').then(function (data) {
            console.log("Types : ", data);
            _this.Types = data;
        });
    }
    RegisterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegisterPage');
    };
    RegisterPage.prototype.doRegister = function () {
        var _this = this;
        console.log("register response1: ");
        this.emailregex = /\S+@\S+\.\S+/;
        if (this.info['name'].length < 3)
            this.Toast.presentToast('הכנס שם מלא');
        else if (this.info['phone'].length < 8)
            this.Toast.presentToast('הכנס מספר טלפון חוקי');
        else if (!this.emailregex.test(this.info['mail'])) {
            this.info['mail'] = '';
            this.Toast.presentToast('מייל לא תקין');
        }
        else if (this.info['address'].length < 2)
            this.Toast.presentToast('הכנס כתובת חוקית');
        else if (this.info['password'].length < 3)
            this.Toast.presentToast('הכנס סיסמה תקינה');
        else if (this.info['area'] == '')
            this.Toast.presentToast('בחר איזור');
        else if (this.info['type'] == '')
            this.Toast.presentToast('בחר סוג תפקיד');
        else {
            this.info['image'] = this.serverImage;
            this.Login.RegisterUser("RegisterUser", this.info).then(function (data) {
                if (data == 0) {
                    _this.Toast.presentToast('ארעה שגיאה , אנא נסה שנית');
                }
                else {
                    window.localStorage.id = data;
                    window.localStorage.name = _this.info['name'];
                    window.localStorage.area = _this.info["area_id"];
                    window.localStorage.id = _this.info["id"];
                    window.localStorage.name = _this.info["name"];
                    window.localStorage.info = _this.info["info"];
                    window.localStorage.image = _this.info["image"];
                    window.localStorage.type = _this.info["type_id"];
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__home_home__["a" /* HomePage */]);
                }
            });
        }
    };
    RegisterPage.prototype.GoToLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__login_login__["a" /* LoginPage */]);
    };
    /// Image Upload
    RegisterPage.prototype.presentActionSheet = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Select Image Source',
            buttons: [
                {
                    text: 'Load from Library',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Use Camera',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    };
    RegisterPage.prototype.takePicture = function (sourceType) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 60,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            targetWidth: 1000,
            targetHeight: 1000,
            allowEdit: true
        };
        // Get the data of an image
        this.camera.getPicture(options).then(function (imagePath) {
            // Special handling for Android library
            console.log("f0");
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                console.log("f1");
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    _this.correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    _this.currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copyFileToLocalDir(_this.correctPath, _this.currentName, _this.createFileName());
                });
            }
            else {
                console.log("f1");
                _this.currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                console.log("f2 : ", _this.currentName);
                _this.correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                console.log("f3 : ", _this.correctPath);
                _this.copyFileToLocalDir(_this.correctPath, _this.currentName, _this.createFileName());
                console.log("f4");
            }
        }, function (err) {
            _this.presentToast('Error while selecting image.');
        });
    };
    // Create a new name for the image
    RegisterPage.prototype.createFileName = function () {
        console.log("f1");
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    // Copy the image to a local folder
    RegisterPage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        console.log("f2 : " + namePath + " : " + currentName + " : " + newFileName);
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function (success) {
            _this.lastImage = newFileName;
            _this.uploadImage();
        }, function (error) {
            _this.presentToast('Error while storing file.');
        });
    };
    RegisterPage.prototype.presentToast = function (text) {
        console.log("f3");
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'bottom',
            cssClass: "ToastClass"
        });
        toast.present();
    };
    // Always get the accurate path to your apps folder
    RegisterPage.prototype.pathForImage = function (img) {
        if (img === null) {
            return '';
        }
        else {
            return cordova.file.dataDirectory + img;
        }
    };
    RegisterPage.prototype.uploadImage = function () {
        var _this = this;
        // Destination URL
        console.log("Up1  : ");
        var url = "http://www.tapper.co.il/gan/laravel/public/api/GetFile";
        // File for Upload
        console.log("Up2  : " + this.lastImage + " : " + this.pathForImage(this.lastImage));
        var targetPath = this.pathForImage(this.lastImage);
        // File name only
        var filename = this.lastImage;
        var options = {
            fileKey: "file",
            fileName: filename,
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params: { 'fileName': filename }
        };
        var fileTransfer = this.transfer.create();
        this.loading = this.loadingCtrl.create({
            content: 'Uploading...',
        });
        this.loading.present();
        // Use the FileTransfer to upload the image
        console.log("Up1  : ", targetPath + " : " + url + " : " + options);
        fileTransfer.upload(targetPath, url, options).then(function (data) {
            console.log("Updata  : ", data.response);
            _this.serverImage = data.response;
            _this.loading.dismissAll();
        }, function (err) {
            _this.loading.dismissAll();
            _this.presentToast('Error while uploading file.');
        });
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-register',template:/*ion-inline-start:"C:\Users\USER\Documents\github\zahala\src\pages\register\register.html"*/'<ion-content padding>\n\n  <div class="loginMain">\n\n    <div class="loginTitle">\n\n      הרשמה\n\n    </div>\n\n    <div class="InputsText" align="center">\n\n      <div class="InputText">\n\n        <input type="text" placeholder="הכנס שם מלא" [(ngModel)]="info.name" />\n\n      </div>\n\n      <div class="InputText mt10">\n\n        <input type="text" placeholder="הכנס מספר טלפון" [(ngModel)]="info.phone" />\n\n      </div>\n\n      <div class="InputText mt10">\n\n        <input type="text" placeholder="הכנס כתובת מדוייקת" [(ngModel)]="info.address" />\n\n      </div>\n\n      <div class="InputText mt10">\n\n        <input type="text" placeholder="הכנס אימייל" [(ngModel)]="info.mail" />\n\n      </div>\n\n      <div class="InputText mt10">\n\n        <input type="text" placeholder="הכנס סיסמה" [(ngModel)]="info.password" />\n\n      </div>\n\n\n\n      <ion-item class="selectRegion">\n\n        <ion-label>בחר איזור</ion-label>\n\n        <ion-select [(ngModel)]="info.area" >\n\n          <ion-option value="{{region.id}}" *ngFor="let region of Regions">{{region.name}}</ion-option>\n\n        </ion-select>\n\n      </ion-item>\n\n\n\n      <ion-item class="selectRegion">\n\n        <ion-label>בחר סוג</ion-label>\n\n        <ion-select [(ngModel)]="info.type">\n\n          <ion-option value="{{type.id}}" *ngFor="let type of Types">{{type.name}}</ion-option>\n\n        </ion-select>\n\n      </ion-item>\n\n\n\n      <div class="InputText mt10">\n\n        <textarea [(ngModel)]="info.info" name="info" placeholder="הכניסי קצת פרטים עלייך" rows="4"></textarea>\n\n      </div>\n\n\n\n      <div style="width: 100%; margin-top: 20px;" align="center" (click)="presentActionSheet()">\n\n        <div style="width: 90%">\n\n          <button ion-button color="primary" style="width: 100%; color: white">הוסף תמונה</button>\n\n        </div>\n\n      </div>\n\n\n\n      <button class="loginButton" ion-button block (click)="doRegister()">התחל</button>\n\n      <a class="register" (click)="GoToLogin()">חזור להתחברות  </a>\n\n    </div>\n\n  </div>\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\zahala\src\pages\register\register.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_loginService__["a" /* loginService */], __WEBPACK_IMPORTED_MODULE_4__services_toastService__["a" /* toastService */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_diagnostic__["a" /* Diagnostic */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_file_transfer__["a" /* FileTransfer */], __WEBPACK_IMPORTED_MODULE_9__ionic_native_file__["a" /* File */], __WEBPACK_IMPORTED_MODULE_10__ionic_native_file_path__["a" /* FilePath */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* LoadingController */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 170:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_add_note_modal_add_note_modal__ = __webpack_require__(331);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_sent_to_server_service__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the NotesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NotesPage = (function () {
    function NotesPage(navCtrl, navParams, modalCtrl, serverService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.serverService = serverService;
        this.Notes = [];
        this.getAllNotes();
    }
    NotesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NotesPage');
    };
    NotesPage.prototype.openModal = function () {
        var _this = this;
        var noteModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_add_note_modal_add_note_modal__["a" /* AddNoteModalComponent */]);
        noteModal.present();
        noteModal.onDidDismiss(function (data) {
            if (data.type == 1) {
                _this.serverService.addNote('addNote', data).then(function (data) {
                    _this.Notes = data;
                });
            }
        });
    };
    NotesPage.prototype.updateModal = function (i) {
        var _this = this;
        var noteModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_add_note_modal_add_note_modal__["a" /* AddNoteModalComponent */], {
            title: this.Notes[i]['title'],
            info: this.Notes[i]['info']
        });
        noteModal.present();
        noteModal.onDidDismiss(function (data) {
            _this.serverService.updateNote('updateNote', data, _this.Notes[i]['id']).then(function (data) {
                _this.Notes = data;
            });
        });
    };
    NotesPage.prototype.getAllNotes = function () {
        var _this = this;
        this.serverService.getAllNotes('getAllNotes').then(function (data) {
            _this.Notes = data;
        });
    };
    NotesPage.prototype.deleteNote = function (id) {
        var _this = this;
        this.serverService.deleteNote('deleteNote', id).then(function (data) {
            _this.Notes = data;
        });
    };
    NotesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-notes',template:/*ion-inline-start:"C:\Users\USER\Documents\github\zahala\src\pages\notes\notes.html"*/'<!--\n\n  Generated template for the NotesPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation. (click)="deleteNote(note.id)"\n\n-->\n\n<ion-header xmlns="http://www.w3.org/1999/html">\n\n\n\n    <ion-navbar>\n\n        <button class="HeaderMenuButton" ion-button icon-only menuToggle end>\n\n            <ion-icon name="md-menu"></ion-icon>\n\n        </button>\n\n        <ion-title align="center" class="HeaderTitle"> צהלה <span class="HeaderSubTitle">תבניות</span></ion-title>\n\n        <ion-buttons>\n\n            <button ion-button icon-only (click)="openModal()" class="HeaderLeftButton">\n\n                <ion-icon name="ios-add-circle-outline"></ion-icon>\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n    <button ion-button block (click)="openModal()"> הוסיפי תבנית</button>\n\n\n\n    <div>\n\n        <ion-list>\n\n            <ion-item text-wrap detail-push *ngFor="let note of Notes let i=index " side="right" style="direction:rtl">\n\n\n\n                <div class="time">\n\n                    <div class="timeLeft">\n\n                        <h3>  {{note.title}}</h3>\n\n                        <h2>{{note.info}}</h2>\n\n                    </div>\n\n                    <div class="timeRight" >\n\n                        <img src="images/del.png" style="width:48%" class="deleteIcon" (click)="deleteNote(note.id)"/>\n\n                        <!--<ion-icon name="ios-create-outline" (click)="updateModal(i)" class="deleteIcon"></ion-icon>-->\n\n                        <!--<ion-icon name="ios-trash-outline" (click)="deleteNote(note.id)" class="deleteIcon"></ion-icon>-->\n\n                    </div>\n\n                    <div class="timeRight" >\n\n                        <img src="images/edit.png" style="width:48%; margin-left:-10px;" class="deleteIcon" (click)="updateModal(i)"/>\n\n                    </div>\n\n                </div>\n\n            </ion-item>\n\n        </ion-list>\n\n    </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\zahala\src\pages\notes\notes.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */], __WEBPACK_IMPORTED_MODULE_3__services_sent_to_server_service__["a" /* sent_to_server_service */]])
    ], NotesPage);
    return NotesPage;
}());

//# sourceMappingURL=notes.js.map

/***/ }),

/***/ 183:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 183;

/***/ }),

/***/ 227:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/add-job/add-job.module": [
		710,
		11
	],
	"../pages/all-substitute/all-substitute.module": [
		711,
		10
	],
	"../pages/chat/chat.module": [
		721,
		9
	],
	"../pages/info/info.module": [
		712,
		0
	],
	"../pages/login/login.module": [
		713,
		8
	],
	"../pages/messages/messages.module": [
		714,
		7
	],
	"../pages/notes/notes.module": [
		715,
		6
	],
	"../pages/open-jobs/open-jobs.module": [
		716,
		4
	],
	"../pages/open-jobs1/open-jobs1.module": [
		717,
		5
	],
	"../pages/project-bids/project-bids.module": [
		718,
		3
	],
	"../pages/register/register.module": [
		719,
		2
	],
	"../pages/user-jobs/user-jobs.module": [
		720,
		1
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 227;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 321:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddJobModalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_toastService__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_loginService__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_config__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the AddJobModalComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 * https://github.com/VitaliiBlagodir/cordova-plugin-datepicker
 */
var AddJobModalComponent = (function () {
    function AddJobModalComponent(viewCtrl, Toast, navParams, Login, Settings) {
        this.viewCtrl = viewCtrl;
        this.Toast = Toast;
        this.navParams = navParams;
        this.Login = Login;
        this.Settings = Settings;
        this.Regions = [];
        this.Types = [];
        this.SortType = [];
        this.Regions = Settings.Regions;
        this.Types = Settings.Types;
        for (var _i = 0, _a = this.Types; _i < _a.length; _i++) {
            var type = _a[_i];
            if (type.type == 2)
                this.SortType.push(type);
        }
        console.log("Tp : ", this.Types);
    }
    AddJobModalComponent.prototype.onSubmit = function (form, type) {
        if (type == 1) {
            if (form.value.date == undefined)
                this.Toast.presentToast('חובה לבחור תאריך');
            else if (form.value.time == undefined)
                this.Toast.presentToast('חובה לבחור שעה');
            else if (form.value.info == undefined)
                this.Toast.presentToast('חובה להזין תיאור המשרה');
            else if (form.value.region == undefined)
                this.Toast.presentToast('חובה לבחור איזור בארץ');
            else if (form.value.type == undefined)
                this.Toast.presentToast('חובה להזין סוג משרה');
            else {
                var data = { 'date': form.value.date, 'time': form.value.time, 'info': form.value.info, 'type': type, 'ganType': form.value.type, 'region': form.value.region };
                this.viewCtrl.dismiss(data);
            }
        }
        else {
            var data = { 'date': form.value.date, 'time': form.value.time, 'info': form.value.info, 'type': type };
            this.viewCtrl.dismiss(data);
        }
    };
    AddJobModalComponent.prototype.ionViewWillEnter = function () {
        this.job = this.navParams.get('job');
        console.log("TT : ", this.job);
        if (this.job) {
            this.date = this.job.Date2;
            this.time = this.job.Time2;
            this.info = this.job.info;
        }
    };
    AddJobModalComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'add-job-modal',template:/*ion-inline-start:"C:\Users\USER\Documents\github\zahala\src\components\add-job-modal\add-job-modal.html"*/'<!-- Generated template for the AddJobModalComponent component -->\n\n<div class="Content">\n\n    <div class="closeButton" (click)="onSubmit(f,0)">\n\n        <button ion-button round class="Bt">x</button>\n\n    </div>\n\n  <div class="title">\n\n    הכנסת משרה\n\n  </div>\n\n\n\n    <form #f="ngForm" class="Form"  >\n\n        <ion-list>\n\n\n\n            <ion-item class="formElement">\n\n                <ion-label>בחר תאריך</ion-label>\n\n                <ion-datetime displayFormat="DD/MM/YYYY" [(ngModel)]="date" name="date"></ion-datetime>\n\n            </ion-item>\n\n\n\n            <ion-item class="formElement">\n\n                <ion-label>בחר שעה</ion-label>\n\n                <ion-datetime displayFormat="HH:mm " pickerFormat="HH mm " [(ngModel)]="time" name="time"></ion-datetime>\n\n            </ion-item>\n\n\n\n            <ion-item>\n\n                <ion-label>בחרי איזור</ion-label>\n\n                <ion-select [(ngModel)]="region" name="region">\n\n                    <ion-option value="{{type.id}}" *ngFor="let type of Regions">{{type.name}}</ion-option>\n\n                </ion-select>\n\n            </ion-item>\n\n\n\n            <ion-item>\n\n                <ion-label>בחרי סוג</ion-label>\n\n                <ion-select [(ngModel)]="type" name="type">\n\n                    <ion-option value="{{type.id}}" *ngFor="let type of SortType">{{type.name}}</ion-option>\n\n                </ion-select>\n\n            </ion-item>\n\n\n\n            <ion-item>\n\n                <ion-textarea [(ngModel)]="info" name="info" placeholder="הכניסי תיאור המשרה"></ion-textarea>\n\n            </ion-item>\n\n        </ion-list>\n\n\n\n        <div style="width: 100%; margin-top: 50px;" align="center">\n\n            <div style="width: 80%" align="center">\n\n                <button ion-button block (click)="onSubmit(f,1)">הוסף משרה </button>\n\n                <button ion-button block (click)="onSubmit(f,0)" style="background-color: red">בטל משרה </button>\n\n            </div>\n\n        </div>\n\n\n\n    </form>\n\n</div>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\zahala\src\components\add-job-modal\add-job-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__services_toastService__["a" /* toastService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__services_loginService__["a" /* loginService */], __WEBPACK_IMPORTED_MODULE_4__services_config__["a" /* Config */]])
    ], AddJobModalComponent);
    return AddJobModalComponent;
}());

//# sourceMappingURL=add-job-modal.js.map

/***/ }),

/***/ 322:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return substituteService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var substituteService = (function () {
    function substituteService(http, Settings) {
        this.http = http;
        this.Settings = Settings;
        this.ServerUrl = Settings.ServerUrl;
    }
    ;
    substituteService.prototype.getAllSubstitute = function (url, type) {
        var body = new FormData();
        body.append('type', type);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    substituteService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config__["a" /* Config */]])
    ], substituteService);
    return substituteService;
}());

;
//# sourceMappingURL=substituteService.js.map

/***/ }),

/***/ 323:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Data; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var Data = (function () {
    function Data(http) {
        this.http = http;
        this.items = [
            { title: 'one' },
            { title: 'two' },
            { title: 'three' },
            { title: 'four' },
            { title: 'five' },
            { title: 'six' }
        ];
    }
    Data.prototype.filterItems = function (searchTerm) {
        return this.items.filter(function (item) {
            return item.title.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        });
    };
    Data = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], Data);
    return Data;
}());

//# sourceMappingURL=data.js.map

/***/ }),

/***/ 324:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SendBidModalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_toastService__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_config__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the SendBidModalComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var SendBidModalComponent = (function () {
    function SendBidModalComponent(viewCtrl, navParams, Toast, Settings) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.Toast = Toast;
        this.Settings = Settings;
        this.title1 = "ss";
        this.title1 = window.localStorage.info;
    }
    SendBidModalComponent.prototype.onSubmit = function (form, type) {
        console.log("tt : ", form.value.title);
        if (type == 1) {
            if (form.value.title == undefined)
                this.Toast.presentToast('חובה להזין תיאור משרה');
            else {
                var data = { 'title': form.value.title, 'type': type };
                this.viewCtrl.dismiss(data);
            }
        }
        else {
            var data = { 'title': form.value.title, 'info': form.value.info, 'type': type };
            this.viewCtrl.dismiss(data);
        }
    };
    SendBidModalComponent.prototype.ionViewWillEnter = function () {
        this.info = this.navParams.get('info');
        this.title1 = this.navParams.get('title');
        console.log("TT : ", this.info + " : " + this.title1);
    };
    SendBidModalComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'send-bid-modal',template:/*ion-inline-start:"C:\Users\USER\Documents\github\zahala\src\components\send-bid-modal\send-bid-modal.html"*/'<!-- Generated template for the AddJobModalComponent component -->\n\n<div class="Content">\n\n  <div class="title">\n\n  שלחי הצעה\n\n  </div>\n\n\n\n  <form #f="ngForm" class="Form" >\n\n    <ion-list>\n\n      <ion-item style="text-align: right !important;">\n\n        <ion-textarea  [(ngModel)]="title1" type="text"  name="title" style="text-align: right !important; direction: rtl;" placeholder="כתבי פרטי ההצעה"></ion-textarea >\n\n      </ion-item>\n\n    </ion-list>\n\n\n\n    <div style="width: 100%; margin-top: 50px;" align="center">\n\n      <div style="width: 80%" align="center">\n\n        <button type="submit" ion-button block (click)="onSubmit(f,1)">שלחי הצעה</button>\n\n        <button ion-button block (click)="onSubmit(f,0)" style="background-color: red">סגרי הצעה </button>\n\n      </div>\n\n    </div>\n\n\n\n  </form>\n\n</div>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\zahala\src\components\send-bid-modal\send-bid-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1__services_toastService__["a" /* toastService */], __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */]])
    ], SendBidModalComponent);
    return SendBidModalComponent;
}());

//# sourceMappingURL=send-bid-modal.js.map

/***/ }),

/***/ 331:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddNoteModalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_toastService__ = __webpack_require__(48);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the AddJobModalComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 * https://github.com/VitaliiBlagodir/cordova-plugin-datepicker
 */
var AddNoteModalComponent = (function () {
    function AddNoteModalComponent(viewCtrl, navParams, Toast) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.Toast = Toast;
    }
    AddNoteModalComponent.prototype.onSubmit = function (form, type) {
        if (type == 1) {
            if (form.value.title == undefined)
                this.Toast.presentToast('חובה לבחור נושא');
            else if (form.value.info == undefined)
                this.Toast.presentToast('חובה להזין תיאור תבנית');
            else {
                var data = { 'title': form.value.title, 'info': form.value.info, 'type': type };
                this.viewCtrl.dismiss(data);
            }
        }
        else {
            var data = { 'title': form.value.title, 'info': form.value.info, 'type': type };
            this.viewCtrl.dismiss(data);
        }
    };
    AddNoteModalComponent.prototype.ionViewWillEnter = function () {
        this.info = this.navParams.get('info');
        this.title = this.navParams.get('title');
        console.log("TT : ", this.info + " : " + this.title);
    };
    AddNoteModalComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'add-job-modal',template:/*ion-inline-start:"C:\Users\USER\Documents\github\zahala\src\components\add-note-modal\add-note-modal.html"*/'<!-- Generated template for the AddJobModalComponent component -->\n\n<div class="Content">\n\n  <div class="title">\n\n    הכנסת תבנית\n\n  </div>\n\n\n\n    <form #f="ngForm" class="Form" >\n\n        <ion-list>\n\n            <ion-item style="text-align: right !important;">\n\n                <ion-input [(ngModel)]="title" name="title" style="text-align: right !important; direction: rtl;" placeholder="הכניסי נושא התבנית"></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n                <ion-textarea [(ngModel)]="info" name="info" placeholder="הכניסי תיאור התבנית"></ion-textarea>\n\n            </ion-item>\n\n        </ion-list>\n\n\n\n        <div style="width: 100%; margin-top: 50px;" align="center">\n\n            <div style="width: 80%" align="center">\n\n                <button type="submit" ion-button block (click)="onSubmit(f,1)">הוסף תבנית </button>\n\n                <button ion-button block (click)="onSubmit(f,0)" style="background-color: red">בטל תבנית </button>\n\n            </div>\n\n        </div>\n\n\n\n    </form>\n\n</div>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\zahala\src\components\add-note-modal\add-note-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_toastService__["a" /* toastService */]])
    ], AddNoteModalComponent);
    return AddNoteModalComponent;
}());

//# sourceMappingURL=add-note-modal.js.map

/***/ }),

/***/ 375:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OpenJobs1Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_JobsService__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_config__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_sent_to_server_service__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the OpenJobs1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var OpenJobs1Page = (function () {
    function OpenJobs1Page(navCtrl, zone, navParams, jobsService, Settings, serverService, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.zone = zone;
        this.navParams = navParams;
        this.jobsService = jobsService;
        this.Settings = Settings;
        this.serverService = serverService;
        this.alertCtrl = alertCtrl;
        this.CurrentJob = '';
        this.Notes = [];
        this.Regions = [];
        this.Types = [];
        this.SortType = [];
        this.Area = '';
        this.Type = '';
        this.host = '';
        this.Regions = Settings.Regions;
        this.Types = Settings.Types;
        this.host = Settings.host;
        this.jobs = this.jobsService.OpenJobs;
        for (var _i = 0, _a = this.Types; _i < _a.length; _i++) {
            var type = _a[_i];
            if (type.type == 2)
                this.SortType.push(type);
        }
        this.jobsService._OpenJobs.subscribe(function (data) {
            _this.zone.run(function () {
                console.log("SubScribe : ", data);
                _this.jobs = data;
            });
        });
        console.log("Jobs : ", this.SortType, this.Regions);
        // this.jobsService.getAllOpenJobs('getAllOpenJobs').then((data: any) => {
        //     this.jobs = data;
        //     Settings.openJobs = data.length;
        // });
        this.serverService.getAllNotes('getAllNotes').then(function (data) {
            _this.Notes = data;
        });
    }
    OpenJobs1Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad OpenJobsPage');
    };
    OpenJobs1Page.prototype.presentPrompt = function (i) {
        var _this = this;
        console.log("JB ", i, this.jobs[i].id);
        this.CurrentJob = this.jobs[i].id;
        var myAlert = this.alertCtrl.create({
            title: "בחרי תבנית",
            cssClass: 'noteAlert',
            buttons: [
                {
                    text: 'בטלי',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'הגישי הצעה',
                    handler: function (data) {
                        console.log("J1 : ", _this.CurrentJob, _this.Notes[data].id);
                        _this.serverService.addBid('addBid', _this.CurrentJob, _this.Notes[data].id).then(function (data) {
                            _this.jobs[i].isBid = 1;
                        });
                    }
                }
            ]
        });
        for (var i_1 = 0; i_1 < this.Notes.length; i_1++) {
            myAlert.addInput({
                type: 'radio',
                label: this.Notes[i_1]['title'],
                value: i_1.toString()
            });
        }
        myAlert.present();
    };
    OpenJobs1Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-open-jobs1',template:/*ion-inline-start:"C:\Users\USER\Documents\github\zahala\src\pages\open-jobs1\open-jobs1.html"*/'<!--\n\n  Generated template for the OpenJobsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header xmlns="http://www.w3.org/1999/html">\n\n  <ion-navbar >\n\n    <button class="HeaderMenuButton"  ion-button icon-only menuToggle end>\n\n      <ion-icon name="md-menu"></ion-icon>\n\n    </button>\n\n    <ion-title align="center" class="HeaderTitle"> צהלה <span class="HeaderSubTitle">המשרות שלי</span> </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <div>\n\n    <div class="filter">\n\n      <div class="filterRight">\n\n        <ion-item class="selectRegion1">\n\n          <ion-label>בחר איזור</ion-label>\n\n          <ion-select [(ngModel)]="Area">\n\n            <ion-option value="{{area.id}}" *ngFor="let area of Regions">{{area.name}}</ion-option>\n\n          </ion-select>\n\n        </ion-item>\n\n      </div>\n\n      <div class="filterRight">\n\n        <ion-item class="selectRegion1">\n\n          <ion-label> בחרי סוג </ion-label>\n\n          <ion-select [(ngModel)]="Type">\n\n            <ion-option value="{{type.id}}" *ngFor="let type of SortType">{{type.name}}</ion-option>\n\n          </ion-select>\n\n        </ion-item>\n\n      </div>\n\n    </div>\n\n    <ion-list>\n\n      <ion-item  text-wrap detail-push *ngFor="let job of jobs let i=index" side="right" style="direction:rtl">\n\n        <div class="jobRight">\n\n          <img *ngIf="job.Image == \'\'" class="thumb" src="https://wordsmith.org/words/images/avatar2_large.png" style="width: 100%" />\n\n          <img *ngIf="job.Image != \'\'" class="thumb" src="{{host}}{{job.Image}}" style="width: 100%" />\n\n        </div>\n\n        <div class="jobLeft">\n\n          <div class="time">\n\n            <div class="timeLeft">\n\n              <h3> תאריך : {{job.Date2}}</h3>\n\n            </div>\n\n            <div class="timeRight">\n\n              <h3 style="direction: rtl">שעה : {{job.Time2}}</h3>\n\n            </div>\n\n          </div>\n\n          <hr style="margin:0px;">\n\n          <h2>{{job.info}}</h2>\n\n\n\n          <button *ngIf="job.isBid == 0" ion-button (click)="presentPrompt(i)">הגישי הצעה</button>\n\n          <button *ngIf="job.isBid != 0" ion-button>נשלחה הצעה לעבודה</button>\n\n        </div>\n\n      </ion-item>\n\n    </ion-list>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\zahala\src\pages\open-jobs1\open-jobs1.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_JobsService__["a" /* JobsService */], __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_4__services_sent_to_server_service__["a" /* sent_to_server_service */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], OpenJobs1Page);
    return OpenJobs1Page;
}());

//# sourceMappingURL=open-jobs1.js.map

/***/ }),

/***/ 376:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(377);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(381);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 381:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(701);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_list_list__ = __webpack_require__(702);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__ = __webpack_require__(371);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__ = __webpack_require__(372);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_login_login__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_loginService__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_config__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_http__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_all_substitute_all_substitute__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_user_jobs_user_jobs__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_messages_messages__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_tabs_tabs__ = __webpack_require__(703);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__components_add_job_modal_add_job_modal__ = __webpack_require__(321);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_date_picker__ = __webpack_require__(704);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__services_JobsService__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_register_register__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__services_toastService__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__services_substituteService__ = __webpack_require__(322);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_open_jobs_open_jobs__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_notes_notes__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__components_add_note_modal_add_note_modal__ = __webpack_require__(331);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__services_sent_to_server_service__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_project_bids_project_bids__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_chat_chat__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__services_chatService__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__ionic_native_file_transfer__ = __webpack_require__(326);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__ionic_native_file__ = __webpack_require__(329);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__ionic_native_file_path__ = __webpack_require__(330);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__ionic_native_push__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__ionic_native_firebase__ = __webpack_require__(374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__ionic_native_camera__ = __webpack_require__(327);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__ionic_native_diagnostic__ = __webpack_require__(328);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_open_jobs1_open_jobs1__ = __webpack_require__(375);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__components_send_bid_modal_send_bid_modal__ = __webpack_require__(324);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__providers_data_data__ = __webpack_require__(705);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__providers_data__ = __webpack_require__(323);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__pages_add_job_add_job__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__directives_autosize_autosize__ = __webpack_require__(707);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__ionic_native_media__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__pipes_pipes_module__ = __webpack_require__(708);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












































var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_all_substitute_all_substitute__["a" /* AllSubstitutePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_user_jobs_user_jobs__["a" /* UserJobsPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_messages_messages__["a" /* MessagesPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_16__components_add_job_modal_add_job_modal__["a" /* AddJobModalComponent */],
                __WEBPACK_IMPORTED_MODULE_24__components_add_note_modal_add_note_modal__["a" /* AddNoteModalComponent */],
                __WEBPACK_IMPORTED_MODULE_37__components_send_bid_modal_send_bid_modal__["a" /* SendBidModalComponent */],
                __WEBPACK_IMPORTED_MODULE_19__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_open_jobs_open_jobs__["a" /* OpenJobsPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_notes_notes__["a" /* NotesPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_project_bids_project_bids__["a" /* ProjectBidsPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_open_jobs1_open_jobs1__["a" /* OpenJobs1Page */],
                __WEBPACK_IMPORTED_MODULE_40__pages_add_job_add_job__["a" /* AddJobPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_chat_chat__["a" /* ChatPage */],
                __WEBPACK_IMPORTED_MODULE_41__directives_autosize_autosize__["a" /* AutosizeDirective */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_43__pipes_pipes_module__["a" /* PipesModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], { tabsPlacement: 'top' }, {
                    links: [
                        { loadChildren: '../pages/add-job/add-job.module#AddJobPageModule', name: 'AddJobPage', segment: 'add-job', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/all-substitute/all-substitute.module#AllSubstitutePageModule', name: 'AllSubstitutePage', segment: 'all-substitute', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/info/info.module#InfoPageModule', name: 'InfoPage', segment: 'info', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/messages/messages.module#MessagesPageModule', name: 'MessagesPage', segment: 'messages', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/notes/notes.module#NotesPageModule', name: 'NotesPage', segment: 'notes', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/open-jobs/open-jobs.module#OpenJobsPageModule', name: 'OpenJobsPage', segment: 'open-jobs', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/open-jobs1/open-jobs1.module#OpenJobs1PageModule', name: 'OpenJobs1Page', segment: 'open-jobs1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/project-bids/project-bids.module#ProjectBidsPageModule', name: 'ProjectBidsPage', segment: 'project-bids', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/user-jobs/user-jobs.module#UserJobsPageModule', name: 'UserJobsPage', segment: 'user-jobs', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/chat/chat.module#ChatPageModule', name: 'ChatPage', segment: 'chat', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_11__angular_http__["c" /* HttpModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_all_substitute_all_substitute__["a" /* AllSubstitutePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_user_jobs_user_jobs__["a" /* UserJobsPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_messages_messages__["a" /* MessagesPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_16__components_add_job_modal_add_job_modal__["a" /* AddJobModalComponent */],
                __WEBPACK_IMPORTED_MODULE_24__components_add_note_modal_add_note_modal__["a" /* AddNoteModalComponent */],
                __WEBPACK_IMPORTED_MODULE_37__components_send_bid_modal_send_bid_modal__["a" /* SendBidModalComponent */],
                __WEBPACK_IMPORTED_MODULE_19__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_open_jobs_open_jobs__["a" /* OpenJobsPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_open_jobs1_open_jobs1__["a" /* OpenJobs1Page */],
                __WEBPACK_IMPORTED_MODULE_23__pages_notes_notes__["a" /* NotesPage */],
                __WEBPACK_IMPORTED_MODULE_40__pages_add_job_add_job__["a" /* AddJobPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_project_bids_project_bids__["a" /* ProjectBidsPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_chat_chat__["a" /* ChatPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_28__services_chatService__["a" /* ChatService */],
                __WEBPACK_IMPORTED_MODULE_9__services_loginService__["a" /* loginService */],
                __WEBPACK_IMPORTED_MODULE_18__services_JobsService__["a" /* JobsService */],
                __WEBPACK_IMPORTED_MODULE_25__services_sent_to_server_service__["a" /* sent_to_server_service */],
                __WEBPACK_IMPORTED_MODULE_21__services_substituteService__["a" /* substituteService */],
                __WEBPACK_IMPORTED_MODULE_20__services_toastService__["a" /* toastService */],
                __WEBPACK_IMPORTED_MODULE_10__services_config__["a" /* Config */],
                __WEBPACK_IMPORTED_MODULE_17__ionic_native_date_picker__["a" /* DatePicker */],
                __WEBPACK_IMPORTED_MODULE_34__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_35__ionic_native_diagnostic__["a" /* Diagnostic */],
                __WEBPACK_IMPORTED_MODULE_29__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_30__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_42__ionic_native_media__["a" /* Media */],
                __WEBPACK_IMPORTED_MODULE_31__ionic_native_file_path__["a" /* FilePath */],
                __WEBPACK_IMPORTED_MODULE_32__ionic_native_push__["a" /* Push */],
                __WEBPACK_IMPORTED_MODULE_39__providers_data__["a" /* Data */],
                __WEBPACK_IMPORTED_MODULE_33__ionic_native_firebase__["a" /* Firebase */],
                __WEBPACK_IMPORTED_MODULE_41__directives_autosize_autosize__["a" /* AutosizeDirective */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_38__providers_data_data__["a" /* DataProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 41:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return loginService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var loginService = (function () {
    function loginService(http, Settings) {
        this.http = http;
        this.Settings = Settings;
        this.ServerUrl = Settings.ServerUrl;
    }
    ;
    loginService.prototype.getUserDetails = function (url, info) {
        //console.log("Info : " , info)
        var body = new FormData();
        body.append('mail', info["mail"]);
        body.append('password', info["password"]);
        body.append('push', this.Settings.PushId);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("getUserDetails : ", data); }).toPromise();
    };
    loginService.prototype.logOutUser = function () {
        window.localStorage.identify = '';
        this.Settings.UserId = 0;
    };
    loginService.prototype.getRegions = function (url) {
        var body = new FormData();
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    loginService.prototype.GetTypes = function (url) {
        var body = new FormData();
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    loginService.prototype.RegisterUser = function (url, info) {
        var body = 'info=' + JSON.stringify(info);
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.post(this.ServerUrl + '' + url, body, options).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    loginService.prototype.UpdateUser = function (url, info) {
        var body = 'info=' + JSON.stringify(info);
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.post(this.ServerUrl + '' + url, body, options).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    loginService.prototype.getUserById = function (url) {
        //window.localStorage.id = 14;
        console.log("JobssssId : ", window.localStorage.id);
        var body = new FormData();
        body.append('uid', window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    loginService.prototype.DeleteToken = function (url, id) {
        var body = new FormData();
        body.append('id', id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res; }).do(function (data) { }).toPromise();
    };
    loginService.prototype.UpdatePush = function (url) {
        var body = new FormData();
        body.append('id', window.localStorage.id);
        body.append('push', this.Settings.PushId);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("getUserDetails : ", data); }).toPromise();
    };
    loginService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config__["a" /* Config */]])
    ], loginService);
    return loginService;
}());

;
//# sourceMappingURL=loginService.js.map

/***/ }),

/***/ 47:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JobsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var JobsService = (function () {
    function JobsService(http, Settings) {
        this.http = http;
        this.Settings = Settings;
        this.Jobs = [];
        this._Jobs = new __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__["Subject"]();
        this.Jobs$ = this._Jobs.asObservable();
        this.OpenJobs = [];
        this._OpenJobs = new __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__["Subject"]();
        this.OpenJobs$ = this._OpenJobs.asObservable();
        this.ServerUrl = Settings.ServerUrl;
    }
    ;
    JobsService.prototype.ngOnInit = function () {
        console.log("INIT");
    };
    JobsService.prototype.pushJob = function (bid) {
        var _this = this;
        this.getBidById('getBidById', bid).then(function (data) {
            //alert(JSON.stringify(this.Jobs) + " : length : " + this.Jobs.length )
            for (var i = 0; i < _this.Jobs.length; i++) {
                //alert("Bd : " + this.Jobs[i].id + " :"  + data['jobid'])
                if (_this.Jobs[i].id == data['jobid']) {
                    _this.Jobs[i].bids.push(data);
                    _this._Jobs.next(_this.Jobs);
                }
            }
        });
    };
    JobsService.prototype.getJobById = function (Id) {
        var _this = this;
        console.log("MyId : " + this.ServerUrl + '' + 'getJobById');
        var body = new FormData();
        body.append('id', Id);
        this.http.post(this.ServerUrl + '' + 'getJobById', body).map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.pushOpenJob(data);
        });
    };
    JobsService.prototype.pushOpenJob = function (message) {
        console.log("OJ1 : ", this.OpenJobs);
        //this.OpenJobs.push(message);
        this.OpenJobs.splice(0, 0, message);
        this._OpenJobs.next(this.OpenJobs);
        console.log("OJ2 : ", this.OpenJobs);
    };
    JobsService.prototype.addJob = function (url, info) {
        var _this = this;
        var body = 'uid=' + window.localStorage.id + '&info=' + JSON.stringify(info);
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.post(this.ServerUrl + '' + url, body, options).map(function (res) { return res.json(); }).do(function (data) {
            console.log("SaveMeals : ", data);
            _this.Jobs = data;
            _this._Jobs.next(_this.Jobs);
        }).toPromise();
    };
    JobsService.prototype.getAllJobs = function (url) {
        var _this = this;
        var body = new FormData();
        body.append('id', window.localStorage.id);
        // this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{
        //     console.log("ThisJobs : " , data)
        //     this.Jobs.next(data);
        // })
        console.log("All Jobs");
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) {
            //this.Jobs.next(data);
            console.log("AllJobs : ", data);
            _this.Jobs = data;
            _this._Jobs.next(_this.Jobs);
        }).toPromise();
    };
    JobsService.prototype.getBidById = function (url, id) {
        console.log("MyId : ", id);
        var body = new FormData();
        body.append('id', id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) {
        }).toPromise();
    };
    JobsService.prototype.getAllOpenJobs = function (url) {
        var _this = this;
        var body = new FormData();
        body.append('id', window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) {
            _this.OpenJobs = data;
            _this._OpenJobs.next(_this.OpenJobs);
        }).toPromise();
    };
    JobsService.prototype.UpdateJobState = function (url, id, state) {
        var body = new FormData();
        body.append('id', id);
        body.append('state', state);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res; }).do(function (data) {
            console.log("getUserDetails : ", data);
        }).toPromise();
    };
    JobsService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config__["a" /* Config */]])
    ], JobsService);
    return JobsService;
}());

;
//# sourceMappingURL=JobsService.js.map

/***/ }),

/***/ 48:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return toastService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var toastService = (function () {
    function toastService(toastCtrl) {
        this.toastCtrl = toastCtrl;
        console.log("shay");
    }
    toastService.prototype.presentToast = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'bottom',
            cssClass: 'ToastClass'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    toastService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["p" /* ToastController */]])
    ], toastService);
    return toastService;
}());

//# sourceMappingURL=toastService.js.map

/***/ }),

/***/ 49:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__all_substitute_all_substitute__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__messages_messages__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user_jobs_user_jobs__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_config__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_loginService__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__open_jobs_open_jobs__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_sent_to_server_service__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__src_services_JobsService__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_media__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__services_chatService__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var HomePage = (function () {
    function HomePage(navParams, zone, media, navCtrl, ChatService, jobsService, Settings, Login, Server) {
        var _this = this;
        this.navParams = navParams;
        this.zone = zone;
        this.media = media;
        this.navCtrl = navCtrl;
        this.ChatService = ChatService;
        this.jobsService = jobsService;
        this.Settings = Settings;
        this.Login = Login;
        this.Server = Server;
        this.UserType = 0;
        this.SelectedIndex = 1;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_2__all_substitute_all_substitute__["a" /* AllSubstitutePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_4__user_jobs_user_jobs__["a" /* UserJobsPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_3__messages_messages__["a" /* MessagesPage */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_7__open_jobs_open_jobs__["a" /* OpenJobsPage */];
        this.tab5Root = __WEBPACK_IMPORTED_MODULE_7__open_jobs_open_jobs__["a" /* OpenJobsPage */];
        this.tab6Root = __WEBPACK_IMPORTED_MODULE_2__all_substitute_all_substitute__["a" /* AllSubstitutePage */];
        this.tab7Root = __WEBPACK_IMPORTED_MODULE_2__all_substitute_all_substitute__["a" /* AllSubstitutePage */];
        this.ChatService._messagesLen.subscribe(function (val) {
            _this.zone.run(function () {
                _this.messagesLen = val;
            });
        });
        this.Login.getRegions('GetRegions').then(function (data) {
            console.log("Regions : ", data);
            Settings.Regions = data;
        });
        this.Login.GetTypes('GetTypes').then(function (data) {
            //console.log("Types : ", data);
            Settings.Types = data;
        });
        if (!Settings.User) {
            this.Login.getUserById('getUserById').then(function (data) {
                Settings.User = data[0];
                console.log("ssss  : ", data);
                _this.UserType = Settings.User.type_id == 3 || Settings.User.type_id == 4 ? 2 : 1;
                _this.SelectedIndex = _this.UserType == 2 ? 3 : 2;
                console.log("Jobssss7 : ", data);
                // console.log(this.UserType + " : "  + this.SelectedIndex)
            });
        }
        else {
            this.UserType = Settings.User.type_id == 3 || Settings.User.type_id == 4 ? 2 : 1;
            console.log("Jobssss44 : ");
            this.SelectedIndex = 0; //this.navParams.get('SelectedIndex');
            //console.log(this.UserType  + " : "  + this.SelectedIndex)
        }
        //this.jobsService.getAllJobs('getAllJobs');
        // this.jobsService.getAllJobs('getAllJobs').then((data: any) => {
        //     console.log("Jobssss1 : " , data)
        // });
        this.jobsService.getAllOpenJobs('getAllOpenJobs').then(function (data) {
            console.log("Jobssss2 : ", data);
        });
        console.log("CS : ", ChatService.messagesLen);
        //this.getJobs()
        this.getHomeDetails();
    }
    HomePage.prototype.tabClick = function (tab) {
        console.log("Tab : ", tab);
    };
    HomePage.prototype.getHomeDetails = function () {
        var _this = this;
        this.Server.getHomeDetails('getHomeDetails').then(function (data) {
            //console.log("getHomeDetails : " , data)
            _this.Settings.JobsCount = data.jobs;
            _this.Settings.messages = data.messages;
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\USER\Documents\github\zahala\src\pages\home\home.html"*/'\n\n<!--</ion-content> selectedIndex={{SelectedIndex}}-->\n\n\n\n\n\n    <ion-content padding>\n\n        <ion-tabs *ngIf="UserType == 1" selectedIndex="{{SelectedIndex}}" color="primary">\n\n            <ion-tab  [root]="tab3Root" tabTitle="הודעות" tabIcon="mail" tabIndex=0 tabBadge={{messagesLen}}></ion-tab>\n\n            <ion-tab  [root]="tab2Root" tabTitle="המשרות שלי" tabIndex=1 tabIcon="calendar" tabBadge={{Settings.JobsCount}}></ion-tab>\n\n            <ion-tab [root]="tab6Root" [rootParams]="4"  tabTitle="סייעות מחליפות" tabIndex = 3 tabIcon="calendar" ></ion-tab>\n\n            <ion-tab  [root]="tab1Root" [rootParams]="3" tabTitle="גננות מחליפות" tabIndex=2 tabIcon="people" ></ion-tab>\n\n        </ion-tabs>\n\n\n\n        <ion-tabs *ngIf="UserType == 2" selectedIndex="{{SelectedIndex}}" color="primary">\n\n            <ion-tab [root]="tab3Root" tabTitle="הודעות" tabIcon="mail" tabIndex = 0 tabBadge={{messagesLen}}></ion-tab>\n\n            <ion-tab [root]="tab5Root" [rootParams]="4" tabTitle="משרות סייעות" tabIcon="calendar" tabIndex = 0 tabBadge={{Settings.openJobsType4}}></ion-tab>\n\n            <ion-tab [root]="tab4Root" [rootParams]="3"  tabTitle="משרות גננות" tabIndex = 3 tabIcon="calendar" tabBadge={{Settings.openJobsType3}}></ion-tab>\n\n            <ion-tab [root]="tab7Root"  [rootParams]="2" tabTitle="אלפון" tabIndex = 5 tabIcon="calendar"></ion-tab>\n\n        </ion-tabs>\n\n    </ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\zahala\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_10__ionic_native_media__["a" /* Media */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_11__services_chatService__["a" /* ChatService */], __WEBPACK_IMPORTED_MODULE_9__src_services_JobsService__["a" /* JobsService */], __WEBPACK_IMPORTED_MODULE_5__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_6__services_loginService__["a" /* loginService */], __WEBPACK_IMPORTED_MODULE_8__services_sent_to_server_service__["a" /* sent_to_server_service */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};









//const ServerUrl = "http://tapper.co.il/647/laravel/public/api/";
var ChatService = (function () {
    function ChatService(http, Settings, events, loadingCtrl) {
        this.http = http;
        this.Settings = Settings;
        this.events = events;
        this.loadingCtrl = loadingCtrl;
        this.ChatArray = [];
        this._ChatArray = new __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__["Subject"]();
        this.ChatArray$ = this._ChatArray.asObservable();
        //MessagesArray:any[] = [];
        this._MessagesArray = new __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.MessagesArray$ = this._MessagesArray.asObservable();
        // public _MessagesArray = new Subject<any>();
        // _MessagesArray$: Observable<any> = this._MessagesArray.asObservable();
        this._messagesLen = new __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.messagesLen$ = this._messagesLen.asObservable();
        this.ServerUrl = Settings.ServerUrl;
        console.log(this.ChatArray);
    }
    Object.defineProperty(ChatService.prototype, "MessagesArray", {
        get: function () { return this._MessagesArray.getValue(); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ChatService.prototype, "messagesLen", {
        get: function () { return this._messagesLen.getValue(); },
        enumerable: true,
        configurable: true
    });
    ChatService.prototype.addTitle = function (url, obj) {
        var body = 'info=' + JSON.stringify(obj);
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.post(this.ServerUrl + '' + url, body, options).map(function (res) { return res; }).do(function (data) { console.log("Chat : ", data); }).toPromise();
    };
    ChatService.prototype.updateReadMessage = function (url, sender, reciver) {
        var body = new FormData();
        body.append('sender_id', sender);
        body.append('reciver_id', reciver);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res; }).do(function (data) { }).toPromise();
    };
    ChatService.prototype.getChatDetails = function (url, reciverId) {
        var _this = this;
        var body = new FormData();
        body.append('sender_id', window.localStorage.id);
        body.append('reciver_id', reciverId);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { _this.updateChatArray(data); }).toPromise();
    };
    ChatService.prototype.updateChatArray = function (data) {
        console.log("00002");
        this.ChatArray = data;
        this.ChatArray.reverse();
        this._ChatArray.next(this.ChatArray);
    };
    ChatService.prototype.pushToArray = function (data) {
        this.ChatArray.splice((this.ChatArray.length), 0, data);
        this._ChatArray.next(this.ChatArray);
        console.log(this.ChatArray, this.ChatArray.length);
    };
    ChatService.prototype.registerPush = function (url, pushid) {
        var body = new FormData();
        body.append('push_id', pushid.toString());
        body.append('user_id', window.localStorage.identify.toString());
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return ''; }).do(function (data) { console.log("chat:", data); });
    };
    ChatService.prototype.getChatMessagesCount = function (url) {
        var body = new FormData();
        body.append('user_id', window.localStorage.identify.toString());
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { return data; }).toPromise();
    };
    // getMessages(url)
    // {
    //     let body = new FormData();
    //     body.append('uid', window.localStorage.id);
    //
    //     return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{
    //         this.MessagesArray = data;
    //         this._MessagesArray.next(this.MessagesArray);
    //     }).toPromise();
    // }
    // Get all categories ;
    ChatService.prototype.getMessages = function (url) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        var loading, body, MessagesArray, err_1;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    loading = this.loadingCtrl.create({ content: 'Please wait...' });
                                    loading.present();
                                    _a.label = 1;
                                case 1:
                                    _a.trys.push([1, 3, 4, 5]);
                                    console.log("Local : ", window.localStorage.id);
                                    body = new FormData();
                                    body.append('uid', window.localStorage.id);
                                    return [4 /*yield*/, this.http.post(this.ServerUrl + '' + url, body).toPromise().then(function (response) { return response.json(); })];
                                case 2:
                                    MessagesArray = _a.sent();
                                    this._MessagesArray.next(MessagesArray);
                                    resolve(this.MessagesArray);
                                    return [3 /*break*/, 5];
                                case 3:
                                    err_1 = _a.sent();
                                    console.log(err_1);
                                    reject(err_1);
                                    return [3 /*break*/, 5];
                                case 4:
                                    loading.dismiss();
                                    this.calculateMessagesLength();
                                    return [7 /*endfinally*/];
                                case 5: return [2 /*return*/];
                            }
                        });
                    }); })];
            });
        });
    };
    ChatService.prototype.getMessageById = function (url, id) {
        var body = new FormData();
        body.append('id', id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { return data; }).toPromise();
    };
    ChatService.prototype.addPushMessageToArray = function (data) {
        data.unread_message = 0;
        this.Obj = data; //{ id:'', uid:3 , title:data.title , date:this.newdate , type:1,name:data.SenderName,image:data.SenderImage};
        this.pushToArray(this.Obj);
        this.events.publish('newchat', "newchat");
        var sum = this.messagesLen;
        sum = Number(sum) + 1;
        this._messagesLen.next(sum);
    };
    ChatService.prototype.calculateMessagesLength = function () {
        console.log("MessagesArray : ", this.MessagesArray);
        var sum = 0;
        for (var _i = 0, _a = this.MessagesArray; _i < _a.length; _i++) {
            var item = _a[_i];
            sum += Number(item.unread_message);
        }
        this._messagesLen.next(sum);
        // return new Promise<void>(async (resolve, reject) => {
        //     this._messagesLen.next(sum);
        // });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["d" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["d" /* Content */])
    ], ChatService.prototype, "content", void 0);
    ChatService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_3__config__["a" /* Config */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["j" /* LoadingController */]])
    ], ChatService);
    return ChatService;
}());

;
//# sourceMappingURL=chatService.js.map

/***/ }),

/***/ 53:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return sent_to_server_service; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var sent_to_server_service = (function () {
    function sent_to_server_service(http, Settings) {
        this.http = http;
        this.Settings = Settings;
        this.ServerUrl = Settings.ServerUrl;
    }
    ;
    sent_to_server_service.prototype.addNote = function (url, info) {
        var body = new FormData();
        body.append('title', info["title"]);
        body.append('info', info["info"]);
        body.append('uid', window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("titleInfo : ", data); }).toPromise();
    };
    sent_to_server_service.prototype.updateNote = function (url, info, id) {
        var body = new FormData();
        body.append('title', info["title"]);
        body.append('info', info["info"]);
        body.append('id', id);
        body.append('uid', window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("titleInfo : ", data); }).toPromise();
    };
    sent_to_server_service.prototype.getAllNotes = function (url) {
        var body = new FormData();
        body.append('uid', window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("Notes : ", data); }).toPromise();
    };
    sent_to_server_service.prototype.deleteNote = function (url, id) {
        var body = new FormData();
        body.append('id', id);
        body.append('uid', window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("Notes : ", data); }).toPromise();
    };
    //Bid
    sent_to_server_service.prototype.addBid = function (url, job, info) {
        var body = new FormData();
        body.append('job', job);
        body.append('info', info);
        body.append('uid', window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res; }).do(function (data) { console.log("Notes : ", data); }).toPromise();
    };
    sent_to_server_service.prototype.getHomeDetails = function (url) {
        var body = new FormData();
        body.append('uid', window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    sent_to_server_service.prototype.updateJob = function (url, info, id) {
        var body = new FormData();
        body.append('Date2', info['date']);
        body.append('Time2', info['time']);
        body.append('info', info['info']);
        body.append('id', id);
        body.append('uid', window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("titleInfo : ", data); }).toPromise();
    };
    sent_to_server_service = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config__["a" /* Config */]])
    ], sent_to_server_service);
    return sent_to_server_service;
}());

;
//# sourceMappingURL=sent_to_server_service.js.map

/***/ }),

/***/ 70:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserJobsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_add_job_modal_add_job_modal__ = __webpack_require__(321);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_JobsService__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_config__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__project_bids_project_bids__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_sent_to_server_service__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__add_job_add_job__ = __webpack_require__(166);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the UserJobsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var UserJobsPage = (function () {
    function UserJobsPage(app, zone, navCtrl, navParams, modalCtrl, jobsService, Settings, serverService) {
        var _this = this;
        this.app = app;
        this.zone = zone;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.jobsService = jobsService;
        this.Settings = Settings;
        this.serverService = serverService;
        this.jobs = [];
        console.log("SubScribe22 : ");
        this.jobs = this.jobsService.Jobs;
        this.host = Settings.host;
        this.jobsService._Jobs.subscribe(function (data) {
            _this.zone.run(function () {
                console.log("SubScribe : ", data);
                _this.jobs = data;
            });
        });
    }
    UserJobsPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        console.log("ionViewWillEnterJobssss2");
        this.jobsService.getAllJobs('getAllJobs').then(function (data) {
            _this.jobs = data;
        });
    };
    UserJobsPage.prototype.ngOnInit = function () {
    };
    UserJobsPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        this.jobsService.getAllJobs('getAllJobs').then(function (data) {
            _this.jobs = data;
            refresher.complete();
        });
    };
    UserJobsPage.prototype.openModal = function () {
        //this.app.getRootNav().setRoot(AddJobPage);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__add_job_add_job__["a" /* AddJobPage */]);
        // let jobsModal = this.modalCtrl.create(AddJobModalComponent);
        // jobsModal.present();
        //
        // jobsModal.onDidDismiss(data => {
        //     console.log("jb : " , data);
        //     if(data.type == 1)
        //     {
        //         console.log("send1")
        //         this.jobsService.addJob('addJob',data).then((data: any) => {
        //             console.log("send2 : " + data);
        //             //alert(JSON.stringify(data));
        //             this.jobs = data;
        //         });
        //     }
        // });
    };
    UserJobsPage.prototype.updateModal = function (i) {
        var _this = this;
        var noteModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_add_job_modal_add_job_modal__["a" /* AddJobModalComponent */], {
            job: this.jobs[i]
        });
        noteModal.present();
        noteModal.onDidDismiss(function (data) {
            console.log(data);
            _this.serverService.updateJob('updateJob', data, _this.jobs[i]['id']).then(function (data) {
                _this.jobs = data;
            });
        });
    };
    UserJobsPage.prototype.open_close_project = function (job) {
        job.is_close == 0 ? job.is_close = 1 : job.is_close = 0;
        this.jobsService.UpdateJobState('UpdateJobState', job.id, job.is_close).then(function (data) {
            console.log("JobState : ", data);
        });
    };
    UserJobsPage.prototype.checkState = function (type) {
        var str = (type == 0) ? 'לביטול הפרוייקט' : 'לפתיחת הפרוייקט';
        return str;
    };
    UserJobsPage.prototype.open_project_bid = function (i) {
        this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_5__project_bids_project_bids__["a" /* ProjectBidsPage */], { project: this.jobs[i].bids });
        //this.navCtrl.push(ProjectBidsPage,{project:this.jobs[i].bids});
    };
    UserJobsPage.prototype.reversDate = function (dt) {
        var arr = dt.split('-');
        var str = arr[2] + "/" + arr[1] + "/" + arr[0];
        return str;
    };
    UserJobsPage.prototype.cutTime = function (time) {
        var tm = time.split(":");
        return tm[0] + ":" + tm[1];
    };
    UserJobsPage.prototype.getCurrentDate = function (date) {
        var tm = date.split(" ");
        var tm1 = tm[0].split("-");
        return tm1[2] + "/" + tm1[1] + "/" + tm1[0];
    };
    UserJobsPage.prototype.getCurrentTime = function (date) {
        var tm = date.split(" ");
        var tm1 = tm[1].split(":");
        return tm1[0] + ":" + tm1[1];
    };
    UserJobsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-user-jobs',template:/*ion-inline-start:"C:\Users\USER\Documents\github\zahala\src\pages\user-jobs\user-jobs.html"*/'<!--\n\n  Generated template for the UserJobsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header xmlns="http://www.w3.org/1999/html">\n\n\n\n  <ion-navbar hideBackButton >\n\n    <button class="HeaderMenuButton"  ion-button icon-only menuToggle end>\n\n      <ion-icon name="md-menu"></ion-icon>\n\n    </button>\n\n    <ion-title align="center" class="HeaderTitle">  <span class="HeaderSubTitle">המשרות שלי</span> </ion-title>\n\n    <ion-buttons>\n\n      <button ion-button icon-only (click)="openModal()" class="HeaderLeftButton">\n\n        <ion-icon name="ios-add-circle-outline"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n\n        <ion-refresher-content></ion-refresher-content>\n\n    </ion-refresher>\n\n    <div>\n\n        <ion-list>\n\n          <ion-item  text-wrap detail-push *ngFor="let job of jobs let i=index" side="right" style="direction:rtl">\n\n              <div class="jobRight">\n\n                  <img *ngIf="job.Image == \'\'" class="thumb" src="https://wordsmith.org/words/images/avatar2_large.png" style="width: 100%" />\n\n                  <img *ngIf="job.Image != \'\'" class="thumb" src="{{host}}{{job.Image}}" style="width: 100%" />\n\n              </div>\n\n              <div class="jobLeft">\n\n                  <div class="time">\n\n                      <div class="timeLeft">\n\n\n\n                          <h3>  {{getCurrentDate(job.created_at)}}  </h3>\n\n                      </div>\n\n                      <div class="timeRight">\n\n                          <h3 style="direction: rtl">שעה : {{getCurrentTime(job.created_at)}}</h3>\n\n                      </div>\n\n                      <div class="edit" (click)="updateModal(i)">\n\n                          <img src="images/edit.png" style="width: 100%" />\n\n                      </div>\n\n                  </div>\n\n                  <hr style="margin:0px;">\n\n                  <h2>{{job.info}}</h2>\n\n                  <h2 class="jobDate">תאריך : {{reversDate(job.Date2)}} </h2>\n\n                  <h2 class="jobDate"><span> התחלה : {{job.Time2}}</span>&nbsp;&nbsp;  סיום : {{cutTime(job.end_time)}}&nbsp;</h2>\n\n                  <button ion-button (click)="open_close_project(job)">{{checkState(job.is_close)}}</button>\n\n                  <button ion-button (click)="open_project_bid(i)" style="background-color: red">{{job.bids.length}} הצעות הוגשו</button>\n\n              </div>\n\n          </ion-item>\n\n        </ion-list>\n\n    </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\zahala\src\pages\user-jobs\user-jobs.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */], __WEBPACK_IMPORTED_MODULE_3__services_JobsService__["a" /* JobsService */], __WEBPACK_IMPORTED_MODULE_4__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_6__services_sent_to_server_service__["a" /* sent_to_server_service */]])
    ], UserJobsPage);
    return UserJobsPage;
}());

//# sourceMappingURL=user-jobs.js.map

/***/ }),

/***/ 701:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(371);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(372);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_config__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_notes_notes__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_push__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_JobsService__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_chatService__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_firebase__ = __webpack_require__(374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__services_loginService__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_media__ = __webpack_require__(143);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};














var MyApp = (function () {
    function MyApp(platform, media, ChatService, Login, firebase, alertCtrl, chatService, loginService, jobsService, statusBar, splashScreen, Settings, push, alert) {
        // used for an example of ngFor and navigation
        this.platform = platform;
        this.media = media;
        this.ChatService = ChatService;
        this.Login = Login;
        this.firebase = firebase;
        this.alertCtrl = alertCtrl;
        this.chatService = chatService;
        this.loginService = loginService;
        this.jobsService = jobsService;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.Settings = Settings;
        this.push = push;
        this.alert = alert;
        this.rootPage = ""; //LoginPage;
        this.pages = [
            { title: 'עמוד ראשי', component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */], icon: 'ios-home-outline' },
            { title: 'מי אנחנו', component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */], icon: 'ios-people-outline' },
            { title: 'ערוך פרטים אישיים', component: 'InfoPage', icon: 'ios-create-outline' },
            { title: 'תבנית הערות', component: __WEBPACK_IMPORTED_MODULE_7__pages_notes_notes__["a" /* NotesPage */], icon: 'ios-document-outline' },
            { title: 'התנתק', component: 'LogOut', icon: 'ios-close-circle-outline' }
        ];
        //console.log("Local : ", window.localStorage.id, window.localStorage.sid)
        if (window.localStorage.id == undefined || window.localStorage.id == '')
            this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */]; //RegisterPage;//
        else {
            this.Login.UpdatePush('UpdatePush');
            this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */]; // ChatPage; //
        }
        // setTimeout(function(){
        //   console.log(chatService)
        //     chatService.getMessageById('getMessageById' , 454).then((data: any) => {
        //         console.log("Message : " , data);
        //         chatService.addPushMessageToArray(data);
        //     });
        // }, 4000);
        this.initializeApp();
    }
    MyApp.prototype.initializeApp = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.platform.ready().then(function () {
                            // Okay, so the platform is ready and our plugins are available.
                            // Here you can do any higher level native things you might need.
                            _this.statusBar.styleDefault();
                            _this.splashScreen.hide();
                            console.log("ININ");
                            if (_this.platform.is('cordova')) {
                                _this.initializeFireBaseAndroid();
                            }
                            _this.platform.registerBackButtonAction(function () {
                                if (_this.nav.length() == 1) {
                                    var alert_1 = _this.alertCtrl.create({
                                        title: 'יציאה מהאפליקצייה',
                                        message: 'האם אתה בטוח שברצטנך לצאת מהאפליקצייה',
                                        cssClass: 'alertcss',
                                        buttons: [
                                            { text: "כן", handler: function () { _this.platform.exitApp(); } },
                                            { text: "בטל", role: 'cancel' }
                                        ]
                                    });
                                    alert_1.present();
                                }
                                else
                                    _this.nav.pop();
                            });
                            // setTimeout(()=>{
                            //     this.jobsService.pushJob(161);
                            // }, 4000);
                        });
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.ChatService.getMessages('getMessages')];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        err_1 = _a.sent();
                        console.log('checkTokenOnStart', err_1);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    MyApp.prototype.initializeFireBaseAndroid = function () {
        var _this = this;
        return this.firebase.getToken()
            .catch(function (error) { return console.error('Error getting token', error); })
            .then(function (token) {
            _this.firebase.subscribe('all').then(function (result) {
                if (result)
                    console.log("Subscribed to all");
                _this.subscribeToPushNotificationEvents();
            });
        });
    };
    MyApp.prototype.initializeFireBaseIos = function () {
        var _this = this;
        return this.firebase.grantPermission()
            .catch(function (error) { return console.error('Error getting permission', error); })
            .then(function () {
            _this.firebase.getToken()
                .catch(function (error) { return console.error('Error getting token', error); })
                .then(function (token) {
                console.log("The token is " + token);
                _this.firebase.subscribe('all').then(function (result) {
                    if (result)
                        console.log("Subscribed to all");
                    _this.subscribeToPushNotificationEvents();
                });
            });
        });
    };
    MyApp.prototype.subscribeToPushNotificationEvents = function () {
        var _this = this;
        // Handle token refresh
        this.firebase.onTokenRefresh().subscribe(function (token) {
            _this.Settings.SetUserPush(token);
            _this.saveToken(token);
            // this.loginService.sendToken('GetToken' , token).then((data: any) => {console.log("UserDetails : " , data);});
        }, function (error) {
            console.error('Error refreshing token', error);
        });
        // Handle incoming notifications
        this.firebase.onNotificationOpen().subscribe(function (notification) {
            // !notification.tap
            //     ? alert('The user was using the app when the notification arrived...')
            //     : alert('The app was closed when the notification arrived...');
            if (!notification.tap) {
                if (notification.type == 1) {
                    _this.jobsService.pushJob(notification.Id);
                }
                else if (notification.type == 2) {
                    _this.chatService.getMessageById('getMessageById', notification.id).then(function (data) {
                        _this.chatService.addPushMessageToArray(data);
                    });
                    var file = _this.media.create('https://tapper.co.il/gan/notification.mp3');
                    file.play();
                }
                else if (notification.type == 3) {
                    _this.jobsService.getJobById(notification.Id);
                }
            }
            else {
                if (notification.type == 1)
                    _this.jobsService.pushJob(notification.Id);
                else if (notification.type == 3)
                    _this.jobsService.getJobById(notification.Id);
            }
            // let notificationAlert = this.alertCtrl.create({
            //     title: notification.title,
            //     message: JSON.stringify(notification),
            //     buttons: ['Ok']
            // });
            // notificationAlert.present();
        }, function (error) {
            console.error('Error getting the notification', error);
        });
    };
    MyApp.prototype.saveToken = function (token) {
        // Send the token to the server
        // console.log('Sending token to the server...');
        return Promise.resolve(true);
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.component == 'LogOut') {
            this.loginService.DeleteToken('DeleteToken', window.localStorage.id).then(function (data) { console.log("DeleteToken : ", data); });
            window.localStorage.id = '';
            this.Settings.User = '';
            this.nav.push(__WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */]);
            console.log("Out");
        }
        else
            this.nav.setRoot(page.component);
    };
    MyApp.prototype.pushSetup = function () {
        var _this = this;
        var options = {
            android: {
                senderID: '473000068631'
            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'false'
            },
            windows: {},
            browser: {
                pushServiceURL: 'http://push.api.phonegap.com/v1/push'
            }
        };
        var pushObject = this.push.init(options);
        pushObject.on('notification').subscribe(function (notification) {
            //alert("Push ")
            // let Message =  notification.additionalData.Message;
            // let Type =  notification.additionalData.Type;
            //
            // if(Type == 1)
            //     this.jobsService.pushJob(Message);
            // if(Type == 2)
            //     this.chatService.pushToArray(Message);
            // if(Type == 3)
            //     this.jobsService.pushOpenJob(Message);
            //
            // if(notification.additionalData.foreground) {
            //
            //
            // }
            // else
            // {
            //     //alert("Notification33333 " + JSON.stringify(notification));
            // }
        });
        pushObject.on('registration').subscribe(function (registration) {
            _this.Settings.SetUserPush(registration.registrationId.toString());
        });
        pushObject.on('error').subscribe(function (error) { return console.error('Error with Push plugin', error); });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\USER\Documents\github\zahala\src\app\app.html"*/'<ion-menu [content]="content" side="right">\n\n  <ion-header>\n\n    <ion-toolbar  style="background-color: #022850 !important;">\n\n      <ion-title style="text-align: right;">תפריט צהלה</ion-title>\n\n    </ion-toolbar>\n\n  </ion-header>\n\n\n\n  <ion-content>\n\n    <ion-list>\n\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)" style="text-align: right; direction: rtl">\n\n        <ion-icon class="sideMenuIcon" name="{{p.icon}}"></ion-icon>\n\n        <span class="sideMenuText">{{p.title}}</span>\n\n      </button>\n\n    </ion-list>\n\n  </ion-content>\n\n\n\n</ion-menu>\n\n\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false" ></ion-nav>'/*ion-inline-end:"C:\Users\USER\Documents\github\zahala\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */], __WEBPACK_IMPORTED_MODULE_13__ionic_native_media__["a" /* Media */], __WEBPACK_IMPORTED_MODULE_10__services_chatService__["a" /* ChatService */], __WEBPACK_IMPORTED_MODULE_12__services_loginService__["a" /* loginService */], __WEBPACK_IMPORTED_MODULE_11__ionic_native_firebase__["a" /* Firebase */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_10__services_chatService__["a" /* ChatService */], __WEBPACK_IMPORTED_MODULE_12__services_loginService__["a" /* loginService */], __WEBPACK_IMPORTED_MODULE_9__services_JobsService__["a" /* JobsService */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_6__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_push__["a" /* Push */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 702:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ListPage = (function () {
    function ListPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        // Let's populate this page with some filler content for funzies
        this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
            'american-football', 'boat', 'bluetooth', 'build'];
        this.items = [];
        for (var i = 1; i < 11; i++) {
            this.items.push({
                title: 'Item ' + i,
                note: 'This is item #' + i,
                icon: this.icons[Math.floor(Math.random() * this.icons.length)]
            });
        }
    }
    ListPage_1 = ListPage;
    ListPage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(ListPage_1, {
            item: item
        });
    };
    ListPage = ListPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-list',template:/*ion-inline-start:"C:\Users\USER\Documents\github\zahala\src\pages\list\list.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>List</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-list>\n\n    <button ion-item *ngFor="let item of items" (click)="itemTapped($event, item)">\n\n      <ion-icon [name]="item.icon" item-start></ion-icon>\n\n      {{item.title}}\n\n      <div class="item-note" item-end>\n\n        {{item.note}}\n\n      </div>\n\n    </button>\n\n  </ion-list>\n\n  <div *ngIf="selectedItem" padding>\n\n    You navigated here from <b>{{selectedItem.title}}</b>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\zahala\src\pages\list\list.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */]])
    ], ListPage);
    return ListPage;
    var ListPage_1;
}());

//# sourceMappingURL=list.js.map

/***/ }),

/***/ 703:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__all_substitute_all_substitute__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__messages_messages__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user_jobs_user_jobs__ = __webpack_require__(70);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TabsPage = (function () {
    function TabsPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_2__all_substitute_all_substitute__["a" /* AllSubstitutePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_4__user_jobs_user_jobs__["a" /* UserJobsPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_3__messages_messages__["a" /* MessagesPage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tabs',
            template: "<ion-tabs>\n                    <ion-tab [root]=\"tab1Root\" tabTitle=\"\u05DE\u05D7\u05DC\u05D9\u05E4\u05D5\u05EA\"></ion-tab>\n                    <ion-tab [root]=\"tab2Root\" tabTitle=\"\u05D4\u05DE\u05E9\u05E8\u05D5\u05EA \u05E9\u05DC\u05D9\"></ion-tab>\n                    <ion-tab [root]=\"tab3Root\" tabTitle=\"\u05D4\u05D5\u05D3\u05E2\u05D5\u05EA\" tabIcon=\"star\"></ion-tab>\n                  </ion-tabs>"
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */]])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 705:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(706);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the DataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var DataProvider = (function () {
    function DataProvider(http) {
        this.http = http;
        console.log('Hello DataProvider Provider');
    }
    DataProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], DataProvider);
    return DataProvider;
}());

//# sourceMappingURL=data.js.map

/***/ }),

/***/ 707:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AutosizeDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AutosizeDirective = (function () {
    function AutosizeDirective(element) {
        var _this = this;
        this.element = element;
        this.onInput = function (textArea) {
            _this.adjust();
        };
        this.adjust = function () {
            var ta = _this.element.nativeElement.querySelector("textarea");
            if (ta !== undefined && ta !== null) {
                ta.style.overflow = "hidden";
                ta.style.height = "auto";
                ta.style.height = ta.scrollHeight + "px";
            }
        };
    }
    AutosizeDirective.prototype.ngOnInit = function () {
        var _this = this;
        var waitThenAdjust = function (trial) {
            if (trial > 10) {
                // Give up.
                return;
            }
            var ta = _this.element.nativeElement.querySelector("textarea");
            if (ta !== undefined && ta !== null) {
                _this.adjust();
            }
            else {
                setTimeout(function () {
                    waitThenAdjust(trial + 1);
                }, 0);
            }
        };
        // Wait for the textarea to properly exist in the DOM, then adjust it.
        waitThenAdjust(1);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* HostListener */])("input", ["$event.target"]),
        __metadata("design:type", Object)
    ], AutosizeDirective.prototype, "onInput", void 0);
    AutosizeDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["s" /* Directive */])({
            selector: "ion-textarea[autosize]" // Attribute selector
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], AutosizeDirective);
    return AutosizeDirective;
}());

//# sourceMappingURL=autosize.js.map

/***/ }),

/***/ 708:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PipesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__filter_jobs_filter_jobs__ = __webpack_require__(709);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var PipesModule = (function () {
    function PipesModule() {
    }
    PipesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__filter_jobs_filter_jobs__["a" /* FilterJobsPipe */]],
            imports: [],
            exports: [__WEBPACK_IMPORTED_MODULE_1__filter_jobs_filter_jobs__["a" /* FilterJobsPipe */]]
        })
    ], PipesModule);
    return PipesModule;
}());

//# sourceMappingURL=pipes.module.js.map

/***/ }),

/***/ 709:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterJobsPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var FilterJobsPipe = (function () {
    function FilterJobsPipe() {
    }
    /**
     * Takes a value and makes it lowercase.
     */
    FilterJobsPipe.prototype.transform = function (value, region) {
        console.log("Val ", value);
        if (!value)
            return "";
        if (value.length == 0)
            return value;
        console.log("Len : ", value[0], region);
        var resultArray = [];
        for (var _i = 0, value_1 = value; _i < value_1.length; _i++) {
            var item = value_1[_i];
            if (item.region == region || item.area_id == region) {
                resultArray.push(item);
            }
        }
        return resultArray;
    };
    FilterJobsPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({
            name: 'filterJobs',
        })
    ], FilterJobsPipe);
    return FilterJobsPipe;
}());

//# sourceMappingURL=filter-jobs.js.map

/***/ }),

/***/ 71:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_config__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_chatService__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(49);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the ChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ChatPage = (function () {
    function ChatPage(navCtrl, zone, navParams, ChatService, platform, Settings, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.zone = zone;
        this.navParams = navParams;
        this.ChatService = ChatService;
        this.Settings = Settings;
        this.events = events;
        this.chatText = '';
        this.screenHeight = screen.height - 100 + 'px';
        this.senderId = window.localStorage.id;
        this.senderName = "zila";
        this.ChatService._ChatArray.subscribe(function (val) {
            _this.zone.run(function () {
                _this.ChatArray = val;
                console.log("INN", _this.ChatArray);
            });
        });
        this.reciverId = this.navParams.get('id');
        this.reciverName = this.navParams.get('name');
        this.ChatService.updateReadMessage('updateReadMessage', window.localStorage.id, this.reciverId).then(function (data) {
            console.log("updateReadMessage : ", data);
        });
        console.log("C1 : ", this.ChatService.ChatArray);
        this.phpHost = Settings.host;
        document.addEventListener('resume', function () {
            _this.getChatDetails();
        });
        events.subscribe('newchat', function (user, time) {
            // user and time are the same arguments passed in `events.publish(user, time)`
            if (_this.content._scroll)
                _this.scrollBottom();
        });
    }
    ChatPage.prototype.ngOnInit = function () {
        this.getChatDetails();
        this.scrollBottom();
        console.log("C2 : ", this.ChatService.ChatArray);
        this.innerHeight = (window.screen.height);
        var elm = document.querySelector(".chatContent");
        elm.style.height = (this.innerHeight - (this.innerHeight * 0.23)) + 'px';
    };
    ChatPage.prototype.scrollBottom = function () {
        var _this = this;
        setTimeout(function () {
            var element = document.getElementById("chatContent");
            element.scrollIntoView(true);
            element.scrollTop = element.scrollHeight;
            _this.content.scrollToBottom(0);
        }, 300);
    };
    ChatPage.prototype.scrollToBottom = function () {
        try {
            this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        }
        catch (err) { }
    };
    ChatPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        this.ChatService.getChatDetails('getChatDetails', this.reciverId).then(function (data) {
            console.log("ChatDetails: ", _this.ChatService.ChatArray);
            _this.ChatArray = data;
            _this.scrollBottom();
            refresher.complete();
        });
    };
    ChatPage.prototype.getChatDetails = function () {
        var _this = this;
        this.ChatService.getChatDetails('getChatDetails', this.reciverId).then(function (data) {
            console.log("ChatDetails: ", _this.ChatService.ChatArray);
            _this.ChatArray = data;
            _this.scrollBottom();
        });
        //this.ChatArray = data
    };
    ChatPage.prototype.ionViewDidLoad = function () {
        this.scrollBottom();
    };
    ChatPage.prototype.ionViewWillEnter = function () {
        this.scrollBottom();
    };
    ChatPage.prototype.addChatTitle = function () {
        var _this = this;
        if (this.chatText) {
            var chatText1_1 = this.chatText;
            this.chatText = ' ';
            this.date = new Date();
            this.hours = this.date.getHours();
            this.minutes = this.date.getMinutes();
            this.seconds = this.date.getSeconds();
            if (this.hours < 10)
                this.hours = "0" + this.hours;
            if (this.minutes < 10)
                this.minutes = "0" + this.minutes;
            this.time = this.hours + ':' + this.minutes;
            this.today = new Date();
            this.dd = this.today.getDate();
            this.mm = this.today.getMonth() + 1; //January is 0!
            this.yyyy = this.today.getFullYear();
            if (this.dd < 10) {
                this.dd = '0' + this.dd;
            }
            if (this.mm < 10) {
                this.mm = '0' + this.mm;
            }
            this.today = this.dd + '/' + this.mm + '/' + this.yyyy;
            this.newdate = this.today + ' ' + this.time;
            this.Obj = {
                id: '',
                sender_id: window.localStorage.id,
                SenderName: window.localStorage.name,
                title: chatText1_1,
                date: this.newdate,
                reciver_id: this.reciverId,
                SenderImage: window.localStorage.image,
            };
            console.log(this.ChatService.ChatArray);
            this.ChatService.pushToArray(this.Obj);
            this.ChatService.addTitle('addChatTitle', this.Obj).then(function (data) {
                console.log("Weights : ", data), chatText1_1 = '', _this.content.scrollToBottom();
            });
            this.autoSize(40);
        }
    };
    ChatPage.prototype.getHour = function (DateStr) {
        var Hour = DateStr.split(" ");
        return Hour[1];
    };
    ChatPage.prototype.enterPress = function (keyEvent) {
        if (keyEvent.which === 13) {
            this.addChatTitle();
        }
    };
    ChatPage.prototype.cutDate = function (dt) {
        dt = dt.split(" ");
        return dt[0];
    };
    ChatPage.prototype.goBack = function () {
        if (this.navParams.get('type') == 1)
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */], { SelectedIndex: 1 });
        else if (this.navParams.get('type') == 2)
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */], { SelectedIndex: 0 });
        else if (this.navParams.get('type') == 3)
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */], { SelectedIndex: 2 });
    };
    ChatPage.prototype.autoSize = function (pixels) {
        if (pixels === void 0) { pixels = 0; }
        // let textArea = this.element.nativeElement.getElementsByTagName('title')[0];
        var textArea = document.querySelector(".chatInput");
        textArea.style.overflow = 'hidden';
        textArea.style.height = 'auto';
        if (pixels === 0) {
            textArea.style.height = textArea.scrollHeight + 'px';
        }
        else {
            textArea.style.height = pixels + 'px';
        }
        return;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Content */])
    ], ChatPage.prototype, "content", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('scrollMe'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], ChatPage.prototype, "myScrollContainer", void 0);
    ChatPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-chat',template:/*ion-inline-start:"C:\Users\USER\Documents\github\zahala\src\pages\chat\chat.html"*/'<!--\n\n  Generated template for the ChatPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n--><ion-header xmlns="http://www.w3.org/1999/html">\n\n\n\n  <ion-navbar hideBackButton>\n\n    <ion-title align="center" class="HeaderTitle" style="margin-top:2px;"> צהלה <span class="HeaderSubTitle">צ\'ט</span> </ion-title>\n\n    <ion-buttons left>\n\n      <button ion-button (click)="goBack()" icon-only>\n\n        <ion-icon ios="ios-arrow-back" md="md-arrow-back"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content class="ContentClass">\n\n\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n\n    <ion-refresher-content></ion-refresher-content>\n\n  </ion-refresher>\n\n\n\n  <div class="chatContent" id="chatContent" #scrollMe [scrollTop]="scrollMe.scrollHeight">\n\n\n\n    <div class="chatMessage" *ngFor="let message of ChatArray">\n\n\n\n      <div class="chatMessageRight">\n\n        <img *ngIf="message.SenderImage != \'\'" src="{{phpHost}}{{message.SenderImage}}" class="imageclass"/>\n\n        <img *ngIf="message.SenderImage == \'\'" src="https://wordsmith.org/words/images/avatar2_large.png" class="imageclass"/>\n\n      </div>\n\n      <div class="chatMessageLeft">\n\n        <div class="chatTitle">\n\n          <div class="chatTitleLeft">\n\n            <div class="chatTime">{{getHour(message.date)}}</div>\n\n            <div class="chatDate"> {{cutDate(message.date)}}&nbsp; | &nbsp;  </div>\n\n          </div>\n\n          <div class="chatTitleRight">\n\n            <p>{{message.SenderName}}</p>\n\n          </div>\n\n        </div>\n\n\n\n        <p class="chatMessageLeftP">{{message.title}}</p>\n\n      </div>\n\n\n\n    </div>\n\n\n\n  </div>\n\n</ion-content>\n\n\n\n\n\n\n\n<ion-footer class="footerClass">\n\n    <div class="chatFooterDiv" align="center">\n\n        <div class="chatFooterDivRight">\n\n            <ion-item align="center" class="TextContent">\n\n                <!--<ion-textarea (input)="autoSize()" class="chatInput" id="myInput" rows="1"    autosize placeholder="הוסף הודעה" name="title"  [(ngModel)]="chatText" (keypress)="enterPress($event)"></ion-textarea>-->\n\n                <ion-textarea (input)="autoSize();"  class="chatInput" rows="1"  autosize placeholder="הוסף הודעה" [(ngModel)]="chatText"></ion-textarea>\n\n\n\n            </ion-item>\n\n        </div>\n\n        <!--<div class="chatFooterDivLeft" (click)="addChatTitle()">-->\n\n            <!--<img src="images/addchat.png" class="imageclass"/>-->\n\n        <!--</div>-->\n\n\n\n        <div class="chatFooterDivLeft" (click)="addChatTitle()">\n\n            <div class="chatCircle">\n\n            <ion-icon name="ios-arrow-dropleft" class="sendIcon"></ion-icon>\n\n            </div>\n\n            <!--<img src="images/addchat.png" class="imageclass"/>-->\n\n            </div>\n\n    </div>\n\n\n\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Documents\github\zahala\src\pages\chat\chat.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__services_chatService__["a" /* ChatService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Events */]])
    ], ChatPage);
    return ChatPage;
}());

//# sourceMappingURL=chat.js.map

/***/ }),

/***/ 94:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AllSubstitutePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_substituteService__ = __webpack_require__(322);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_config__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__chat_chat__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_data__ = __webpack_require__(323);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the AllSubstitutePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AllSubstitutePage = (function () {
    function AllSubstitutePage(dataService, app, navCtrl, navParams, substitute, Settings) {
        var _this = this;
        this.dataService = dataService;
        this.app = app;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.substitute = substitute;
        this.Settings = Settings;
        this.Substitute = [];
        this.host = '';
        this.Area = window.localStorage.area;
        this.TotalSub3 = 0;
        this.TotalSub4 = 0;
        this.Regions = [];
        this.SubstituteName = '';
        this.typeName = '';
        this.Id = this.navParams.data;
        console.log("Reg : ", Settings.Regions);
        this.Regions = Settings.Regions;
        if (this.Id == 3 || this.Id == 4)
            this.Type = this.Id;
        else
            this.Type = 1;
        this.substitute.getAllSubstitute('getAllSubstitute', this.Id).then(function (data) {
            _this.Substitute = data;
            _this.host = Settings.host;
            Settings.Substitute = data.length;
            _this.items = _this.filterItems(_this.SubstituteName);
            console.log("Items : ", _this.Substitute);
        });
        this.host = Settings.host;
        this.userName = window.localStorage.name;
        this.userImage = this.host + "" + window.localStorage.image;
        this.typeName = Settings.getTypeName(window.localStorage.type);
        console.log("IMAGE : ", this.userImage);
    }
    AllSubstitutePage.prototype.changeType = function (type) {
        this.Type = type;
    };
    AllSubstitutePage.prototype.ionViewDidLoad = function () {
        if (this.Id == 3 || this.Id == 4)
            this.getTotalJobs();
    };
    AllSubstitutePage.prototype.getTotalJobs = function () {
        console.log("getTotalJobs");
        this.TotalSub3 = 0;
        this.TotalSub4 = 0;
        for (var i = 0; i < this.Substitute.length; i++) {
            if (this.Substitute[i].type_id == 3)
                this.TotalSub3++;
            else
                this.TotalSub4++;
        }
    };
    AllSubstitutePage.prototype.setFilteredItems = function () {
        console.log("Filter : ", this.SubstituteName);
        this.items = this.filterItems(this.SubstituteName);
    };
    AllSubstitutePage.prototype.ionViewWillEnter = function () {
        //this.Type = this.navParams.data;
        if (this.Id == 3 || this.Id == 4) {
            this.Settings.openJobsType3 = this.TotalSub3;
            this.Settings.openJobsType4 = this.TotalSub4;
        }
    };
    AllSubstitutePage.prototype.changeTab = function () {
        //this.navCtrl.parent.select(1);
        console.log("Chat1");
    };
    AllSubstitutePage.prototype.gotoChat = function (i) {
        console.log("Chat1 : ", i, this.Substitute[i]);
        //this.navCtrl.push(ChatPage, {id: this.Substitute[i].id, name: this.Substitute[i].name, type: 3});
        this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_4__chat_chat__["a" /* ChatPage */], { id: this.Substitute[i].id, name: this.Substitute[i].name, type: 3 });
    };
    AllSubstitutePage.prototype.filterItems = function (searchTerm) {
        return this.Substitute.filter(function (item) {
            return item['name'].toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        });
    };
    AllSubstitutePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-all-substitute',template:/*ion-inline-start:"C:\Users\USER\Documents\github\zahala\src\pages\all-substitute\all-substitute.html"*/'<!--\n\n  Generated template for the AllSubstitutePage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<!--<ion-header class="Header">-->\n\n\n\n  <!--<ion-navbar >-->\n\n    <!--<button class="HeaderMenuButton"  ion-button icon-only menuToggle end>-->\n\n      <!--<ion-icon name="md-menu"></ion-icon>-->\n\n    <!--</button>-->\n\n    <!--<ion-title class="HeaderTitle"> צהלה <span class="HeaderSubTitle">עמוד מחליפות</span> </ion-title>-->\n\n  <!--</ion-navbar>-->\n\n<!--</ion-header>-->\n\n\n\n<ion-header xmlns="http://www.w3.org/1999/html">\n\n  <ion-navbar >\n\n    <button class="HeaderMenuButton"  ion-button icon-only menuToggle start>\n\n      <ion-icon name="md-menu"></ion-icon>\n\n    </button>\n\n    <ion-title align="center" class="HeaderTitle1">\n\n      <img class="headerImage" src="{{userImage}}"  />\n\n      <div class="HeaderTitleText"> {{userName }}</div>\n\n      <div class="HeaderTitleDesc"> {{typeName }}</div>\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <div class="Types" *ngIf="Id == 1 || Id == 2">\n\n    <div class="typeRight" (click)="changeType(2)">\n\n      <button ion-button [ngClass]="{\'buttonColor1\' : (Type == 2), \'buttonColor2\':(Type == 1)}">סייעות אם</button>\n\n    </div>\n\n    <div class="typeLeft" (click)="changeType(1)">\n\n      <button ion-button   [ngClass]="{\'buttonColor1\' : (Type == 1), \'buttonColor2\':(Type == 2)}" >גננות אם</button>\n\n    </div>\n\n  </div>\n\n  <div class="filter">\n\n    <div class="filterRight">\n\n      <ion-searchbar [(ngModel)]="SubstituteName" (ionInput)="setFilteredItems()" placeholder="חפש גננת/סייעת" style="text-align: right !important; direction: rtl !important;"></ion-searchbar>\n\n    </div>\n\n    <div class="filterLeft">\n\n        <div class="item item-select">\n\n        <select [(ngModel)]="Area">\n\n          <option style="margin-left: 30px; padding-left: 30px; " value="{{area.id}}" *ngFor="let area of Regions">{{area.name}}</option>\n\n        </select>\n\n      </div>\n\n    </div>\n\n  </div>\n\n  <div>\n\n    <ion-list>\n\n      <div *ngFor="let sub of items | filterJobs:Area let i=index">\n\n        <div ion-item text-wrap detail-push *ngIf="sub.type_id == Type"  side="right" style="direction:rtl;" >\n\n          <div class="jobRight">\n\n            <img *ngIf="sub.image == \'\'" class="thumb" src="https://wordsmith.org/words/images/avatar2_large.png" style="width: 100%" />\n\n            <img *ngIf="sub.image != \'\'" class="thumb" src="{{host}}{{sub.image}}" style="width: 100%" />\n\n          </div>\n\n          <div class="jobLeft">\n\n            <div class="time">\n\n              <div class="timeLeft">\n\n                <h3>  {{sub.name}}</h3>\n\n              </div>\n\n            </div>\n\n            <h2 *ngIf="sub.type_id == 3">  תפקיד : גננת מוסמכת</h2>\n\n            <h2 *ngIf="sub.type_id == 4"> תפקיד : סייעת</h2>\n\n            <h4>{{sub.address}}</h4>\n\n            <button ion-button (click)="gotoChat(i)">שלח הודעה ל{{sub.name}}</button>\n\n          </div>\n\n        </div>\n\n      </div>\n\n    </ion-list>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\zahala\src\pages\all-substitute\all-substitute.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__providers_data__["a" /* Data */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_substituteService__["a" /* substituteService */], __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */]])
    ], AllSubstitutePage);
    return AllSubstitutePage;
}());

//# sourceMappingURL=all-substitute.js.map

/***/ }),

/***/ 95:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MessagesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_chatService__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__chat_chat__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_config__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the MessagesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MessagesPage = (function () {
    function MessagesPage(app, zone, navCtrl, navParams, ChatService, Settings) {
        var _this = this;
        this.app = app;
        this.zone = zone;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.ChatService = ChatService;
        this.Settings = Settings;
        this.messages = [];
        this.typeName = '';
        this.ChatService._MessagesArray.subscribe(function (val) {
            _this.zone.run(function () {
                _this.messages = val;
                console.log("INN", _this.messages);
            });
        });
        this.ChatService._messagesLen.subscribe(function (val) {
            _this.zone.run(function () {
                _this.messagesLen = val;
            });
        });
        this.host = this.Settings.host;
        console.log('UserJobsPage');
        this.userName = window.localStorage.name;
        this.userImage = this.host + "" + window.localStorage.image;
        this.typeName = Settings.getTypeName(window.localStorage.type);
    }
    MessagesPage.prototype.getMesages = function () {
        var _this = this;
        this.ChatService.getMessages('getMessages').then(function (data) {
            console.log("2");
            _this.messages = data;
            _this.messagesLen = _this.messages.length;
            console.log("messages : ", _this.messages);
        });
    };
    MessagesPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        this.ChatService.getMessages('getMessages').then(function (data) {
            console.log("2");
            _this.messages = data;
            _this.messagesLen = _this.messages.length;
            refresher.complete();
        });
    };
    MessagesPage.prototype.ionViewDidLoad = function () {
    };
    MessagesPage.prototype.ionViewWillEnter = function () {
        console.log('getMesages');
        this.getMesages();
    };
    MessagesPage.prototype.gotoChat = function (i) {
        //console.log("chat : " ,this.messages[i].id )
        //this.navCtrl.push(ChatPage, {id: this.messages[i].id, name: this.messages[i].user_name});
        this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_3__chat_chat__["a" /* ChatPage */], { id: this.messages[i].id, name: this.messages[i].user_name, type: 2 });
    };
    MessagesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-messages',template:/*ion-inline-start:"C:\Users\USER\Documents\github\zahala\src\pages\messages\messages.html"*/'<!--&lt;!&ndash;-->\n\n  <!--Generated template for the MessagesPage page.-->\n\n\n\n  <!--See http://ionicframework.com/docs/components/#navigation for more info on-->\n\n  <!--Ionic pages and navigation.-->\n\n<!--&ndash;&gt;-->\n\n<!--<ion-header>-->\n\n  <!--<ion-navbar >-->\n\n    <!--<button class="HeaderMenuButton"  ion-button icon-only menuToggle end>-->\n\n      <!--<ion-icon name="md-menu"></ion-icon>-->\n\n    <!--</button>-->\n\n    <!--<ion-title class="HeaderTitle"> צהלה <span class="HeaderSubTitle">עמוד הודעות</span> </ion-title>-->\n\n  <!--</ion-navbar>-->\n\n<!--</ion-header>-->\n\n<ion-header xmlns="http://www.w3.org/1999/html">\n\n    <ion-navbar >\n\n        <button class="HeaderMenuButton"  ion-button icon-only menuToggle start>\n\n            <ion-icon name="md-menu"></ion-icon>\n\n        </button>\n\n        <ion-title align="center" class="HeaderTitle1">\n\n            <img class="headerImage" src="{{userImage}}"  />\n\n            <div class="HeaderTitleText"> {{userName }}</div>\n\n            <div class="HeaderTitleDesc"> {{typeName }}</div>\n\n        </ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n\n    <ion-refresher-content></ion-refresher-content>\n\n  </ion-refresher>\n\n  <div>\n\n    <ion-list>\n\n      <ion-item  text-wrap detail-push *ngFor="let message of messages let i=index" side="right" style="direction:rtl" >\n\n        <div class="jobRight" >\n\n          <img *ngIf="message.user_image == \'\'" class="thumb" src="https://wordsmith.org/words/images/avatar2_large.png" style="width: 100%" />\n\n          <img *ngIf="message.user_image != \'\'" class="thumb" src="{{host}}{{message.user_image}}" style="width: 100%" />\n\n        </div>\n\n        <div class="jobLeft">\n\n          <div class="time">\n\n\n\n          </div>\n\n          <h2>{{message.user_name}}</h2>\n\n          <h3>מספר הודעות : {{message.unread_message}}</h3>\n\n          <button ion-button (click)="gotoChat(i)">שלח הודעה ל{{message.user_name}}</button>\n\n          <!--<button ion-button (click)="open_close_project(job)">{{checkState(job.is_close)}}</button>-->\n\n          <!--<button ion-button (click)="open_project_bid(i)">{{job.bids.length}} הצעות הוגשו</button>-->\n\n        </div>\n\n      </ion-item>\n\n    </ion-list>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\zahala\src\pages\messages\messages.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_chatService__["a" /* ChatService */], __WEBPACK_IMPORTED_MODULE_4__services_config__["a" /* Config */]])
    ], MessagesPage);
    return MessagesPage;
}());

//# sourceMappingURL=messages.js.map

/***/ }),

/***/ 96:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_loginService__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_config__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__register_register__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_chatService__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = (function () {
    function LoginPage(navCtrl, ChatService, navParams, Login, Settings) {
        this.navCtrl = navCtrl;
        this.ChatService = ChatService;
        this.navParams = navParams;
        this.Login = Login;
        this.Settings = Settings;
        this.info = { mail: "haya@gmail.com", password: '123' };
        this.user = '';
        //console.log(this.info["mail"])
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.checkLogin = function () {
        var _this = this;
        //console.log(this.info);
        this.Login.getUserDetails('getUserDetails', this.info).then(function (data) {
            console.log("UserDetails : ", data[0]);
            _this.user = data[0];
            _this.setUserSettings(data);
        });
    };
    LoginPage.prototype.setUserSettings = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("MMM : ", this.user);
                        window.localStorage.id = this.user["id"];
                        window.localStorage.name = this.user["name"];
                        window.localStorage.info = this.user["info"];
                        window.localStorage.image = this.user["image"];
                        window.localStorage.type = this.user["type_id"];
                        window.localStorage.area = this.user["area_id"];
                        this.Settings.UserConnected = this.user;
                        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
                        return [4 /*yield*/, this.ChatService.getMessages('getMessages')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.GoToRegister = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__register_register__["a" /* RegisterPage */]);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"C:\Users\USER\Documents\github\zahala\src\pages\login\login.html"*/'\n\n<ion-content padding>\n\n  <div class="loginMain">\n\n    <div class="loginTitle">\n\n      התחברות\n\n    </div>\n\n    <div class="InputsText" align="center">\n\n      <div class="InputText">\n\n        <input type="text" placeholder="הכנס מייל תקין" [(ngModel)]="info.mail" />\n\n      </div>\n\n      <div class="InputText mt10">\n\n        <input type="text" placeholder="הכנס סיסמה" [(ngModel)]="info.password" />\n\n      </div>\n\n\n\n      <button class="loginButton" ion-button block (click)="checkLogin()">התחל</button>\n\n      <a class="register" (click)="GoToRegister()">הרשמ/י למערכת</a>\n\n    </div>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\zahala\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_6__services_chatService__["a" /* ChatService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_loginService__["a" /* loginService */], __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ })

},[376]);
//# sourceMappingURL=main.js.map